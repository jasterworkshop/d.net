import std.stdio;

import System.IO;

struct Person
{
	string FirstName;
	string LastName;
	string Profession;
	ubyte Age;

	void Print()
	{
		writefln("Name: %s %s\nProfession: %s\nAge:%s\n", this.FirstName, this.LastName, this.Profession, this.Age);
	}
}

void main()
{
	FileStream FS = new FileStream("Data.bin", FileMode.Create);
	BinaryWriter BW = new BinaryWriter(FS, ByteOrder.Big, false);

	// BW.UsesBOM = false;
	// ^^ This can be true or false.
	/*
	 * If it is true, then a BinaryReader(Which must also have UsesBOM set to true) will be able to correct the endianess of any piece of data(As long as it's read in properly)
	 * If it is false, then a BinaryReader(Which must have it's UsesBOM set to false) may change the endianness of the data to the wrong one.
	 * 
	 * If UsesBOM is false:
	 * 	And the ByteOrder is ByteOrder.Native, no change to the endianness is made
	 *  And the ByteOrder is ByteOrder.Big:
	 * 		And the NativeByteOrder is ByteOrder.Big, no change is made
	 * 		And the NativeByteOrder is ByteOrder.Little, the endianness of read in data will be swapped, however it may not be in the correct endianness since there's no BoM for confirmation
	 * 	And the ByteOrder is ByteOrder.Little:
	 * 		And the NativeByteOrder is ByteOrder.Little, no change is made
	 * 		And the NativeByteOrder is ByteOrder.Big, the endianness of read in data will be swapped, however it may not be in the correct endianness since there's no BoM for confirmation
	 * 
	 * If UsesBOM is true, 1 extra byte is written out with the data given. This byte is written just before the data is. This is not true if only a single byte of data is written(Hopefully <3).
	 * */

	Person ThanksObama = Person("Barrack", "Obama", "President of the United States of America", 200);
	Person JapanesePerson = Person("Oda", "Nobunaga", "Dead", ubyte.max);

	BW.WritePerson(ThanksObama);
	BW.WritePerson(JapanesePerson);
	BW.Close();

	FS = new FileStream("Data.bin", FileMode.Open);
	BinaryReader BR = new BinaryReader(FS, false, true);
	auto Obama = BR.ReadPerson();
	auto Oda = BR.ReadPerson();

	assert(Obama == ThanksObama);
	assert(Oda == JapanesePerson);

	Obama.Print();
	Oda.Print();
}

void WritePerson(BinaryWriter bw, Person person)
{
	// When reading or writing strings, you should specify whether it's a: string, dstring, wstring
	bw.Write!string(person.FirstName);
	bw.Write!string(person.LastName);
	bw.Write!string(person.Profession);
	bw.Write(person.Age);
}

Person ReadPerson(BinaryReader br)
{
	Person ToReturn;

	ToReturn.FirstName = br.ReadString!string();
	ToReturn.LastName = br.ReadString!string();
	ToReturn.Profession = br.ReadString!string();
	ToReturn.Age = br.ReadByte();

	return ToReturn;
}