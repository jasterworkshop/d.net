﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
/// This module is used to replace the .Net events
module System.EventManager;

private
{
	import std.traits;

	import System.ArgumentNullException;
}

/// Generic EventHandler
alias EventHandler = void delegate(Object[] args);

// TODO: Figure out what the hell I'm missing from this class. It can't be that simple to implement it with .Net functionality.
// Maybe store the events by their qualified names? So then you can also remove certain events if needed?
class EventManager(E)
if(isSomeFunction!E)
{
	private
	{
		E[]	_Events;
	}

	public
	{
		/// Constructs a new $(B System.EventManager) that has no initial events
		this()
		{
		}

		/// Constructs a new $(B System.EventManager) that initially uses the events in $(B events)
		/// 
		/// Parameters:
		/// 	events = The events that the $(B System.EventManager) should start off with.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B events) is $(B null)
		this(E[] events)
		{
			if(events is null) throw new ArgumentNullException("events");

			this._Events = events;
		}

		/// Adds $(B event) to the $(B System.EventManager). $(B event) will be called alongside any other added event once $(B System.EventManager.Invoke) is called.
		/// 
		/// Parameters:
		/// 	event = The event to add.
		/// 
		/// 
		/// Throws $(B System.ArugmentNullException) if $(B event) is null.
		void AddEvent(E event)
		{
			if(event is null) throw new ArgumentNullException("event");

			this._Events ~= event;
		}

		/// Invokes every registered function with $(B args) passed to them, in the order that they were added.
		/// 
		/// Parameters:
		/// 	args = The arguments to pass to the events registered
		void Invoke(T...)(T args)
		{
			foreach(event; this._Events)
			{
				event(args);
			}
		}

		/// Ditto
		void opCall(T...)(T args)
		{
			this.Invoke(args);
		}

		/// Adds $(B eventToAdd)
		void opOpAssign(string Op)(E eventToAdd)
		if(Op == "+")
		{
			this._Events ~= eventToAdd;
		}
	}
}
//void Errors()
unittest
{
	import std.stdio;

	alias TestHandler = void delegate(string arg);
	auto Manager = new EventManager!TestHandler();

	Manager += delegate void(string arg) { writeln("Event 1: ", arg); };
	Manager += delegate void(string arg) { writeln("Event 2: ", arg); };

	Manager("Message");
}
