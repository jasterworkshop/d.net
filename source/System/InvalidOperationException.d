﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.InvalidOperationException;

private 
{
	import std.string : format;

	import System.SystemException;
}

class InvalidOperationException : SystemException
{
	this(string message)
	{
		super(format("Invalid operation: %s", message));
	}
}

