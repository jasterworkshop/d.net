﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.ArgumentException;

private import std.string : format;

class ArgumentException : Exception
{
	this(string paramName)
	{
		super(format("Parameter name: %s", paramName));
	}
	
	this(string paramName, string message)
	{
		super(format("%s\nParameter name: %s", message, paramName));
	}
}