﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.BitConverter;

private
{
	import std.traits;

	import System.ArgumentNullException;
	import System.ArgumentException;
}

public
{
	import System.ByteOrder;
}

/// Converts base data types and objects to an array of bytes, and an array of bytes to base data types and objects.
static class BitConverter
{
	public static
	{
		/// Attempts to turn $(B data) into a ubyte[] using the given $(B System.ByteOrder).
		/// .
		/// The proccess of converting the byte order is simply reversing the byte data, this may change however.
		/// .
		/// $(B Also note) that the returned bytes will start with a single byte denoting the byte order if $(B makeBOM) is true.
		/// 
		/// Parameters:
		/// 	data = The base data type or object(such as a struct) to attempt to turn into bytes.
		/// 	makeBOM = True if the first byte in the returned data denotes the intended byte order of the data.
		/// 	order = The byte order that the return value is expected to be in.
		/// 
		/// Returns:
		/// 	$(B data) as a byte array in the given byte order.
		ubyte[] GetBytes(T)(T data, bool makeBOM = false, ByteOrder order = NativeByteOrder)
		if(!is(T == class))
		{
			static if(isArray!T)
			{
				if(data is null) throw new ArgumentNullException("toAttempt");
				ubyte[] Data = cast(ubyte[])(cast(void[])data);
			}
			else
			{
				ubyte[] Data = cast(ubyte[])(cast(void[])[data]);
			}

			if(order == NativeByteOrder)
			{
				return (makeBOM) ? [cast(ubyte)order] ~ Data : Data;
			}
			else
			{
				return (makeBOM) ? [cast(ubyte)order] ~ Data.reverse : Data.reverse;
			}
		}

		/// Attempts to convert $(B data) into $(B T).
		/// .
		/// $(B data) is expected to start with a byte signifying it's byte order. However, if the first byte isn't a valid value for a $(B ByteOrder), then no byte order conversion is attempted.
		/// 
		/// Parameters:
		/// 	data = The data to attempt to convert to $(B T)
		/// 	hasBOM = If true, then $(B data)'s first byte signifies it's byte order, and $(B AttemptToT) should try and convert it's byte order. If false, then $(B order) will be used to determine if any conversion is needed on the bytes.
		/// 	order = If $(B hasBOM) is false then $(B order) will determine if $(B data)'s bytes shall be converted into another endianness.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B data) is null.
		/// 
		/// Returns:
		/// 	An instance of $(B T) that is stored in $(B data).
		T To(T)(ubyte[] data, bool hasBOM = false, ByteOrder order = NativeByteOrder)
		if(!is(T == class))
		{
			return ToArray!T(data, hasBOM)[0];
		}

		/// Attempts to convert $(B data) into a $(B T)[].
		/// .
		/// $(B data) is expected to start with a byte signifying it's byte order. However, if the first byte isn't a valid value for a $(B ByteOrder), then no byte order conversion is attempted.
		/// 
		/// Parameters:
		/// 	data = The data to attempt to convert to $(B T)
		/// 	hasBOM = If true, then $(B data)'s first byte signifies it's byte order, and $(B AttemptToT) should try and convert it's byte order. If false, then $(B order) will be used to determine if any conversion is needed on the bytes.
		/// 	order = If $(B hasBOM) is false then $(B order) will determine if $(B data)'s bytes shall be converted into another endianness.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B data) is null.
		/// 
		/// Returns:
		/// 	An array of $(B T) that is stored in $(B data).
		T[] ToArray(T)(ubyte[] data, bool hasBOM = false, ByteOrder order = NativeByteOrder)
		if(!is(T == class))
		{
			if(data is null) throw new ArgumentNullException("data");

//			std.stdio.writeln(typeid(T), " ", data, " ", data.length," ", hasBOM);
//			std.stdio.readln();

			if(hasBOM)
			{
				ByteOrder Order = cast(ByteOrder)data[0];

				if(Order == NativeByteOrder)
				{
					return cast(T[])(cast(void[])data[1..$]);
				}
				else
				{
					return cast(T[])(cast(void[])data[1..$].dup.reverse);
				}
			}
			else
			{
				if(order == NativeByteOrder)
				{
					return cast(T[])(cast(void[])data);
				}
				else
				{
					return cast(T[])(cast(void[])data.dup.reverse);
				}
			}
		}

		/// Returns the specified string as an array of bytes.
		/// Parameters:
		/// 	value = String to convert
		/// Returns:
		/// 	An array of bytes
		ubyte[] GetBytes(T)(T value) if(isSomeString!T) { return cast(ubyte[])(cast(void[])value); }

		/// Returns a $(B T) string converted from $(B data).
		/// Parameters:
		/// 	data = The bytes to convert.
		/// Returns:
		/// 	A $(B T)
		T ToString(T)(ubyte[] data) if(isSomeString!T) { return cast(T)(cast(void[])data); }
	}
}
//void Errors()
unittest
{
	import std.string : format;

	ubyte[] Get(T)(T value)
	{
		return BitConverter.GetBytes(value, true, ByteOrder.Big);
	}

	ubyte[] BoolByte = BitConverter.GetBytes(false);
	ubyte[] CharByte = BitConverter.GetBytes('s');
	ubyte[] ShortBytes = Get(cast(short)0xFECB);
	ubyte[] IntBytes = Get(0xFECBA984);
	ubyte[] LongBytes = Get(0xFECBA984012345FF);
	ubyte[] StringBytes = BitConverter.GetBytes("Soup");

	assert(BoolByte == [0]);
	assert(CharByte == ['s']);
	assert(ShortBytes == [0x01, 0xFE, 0xCB]);
	assert(IntBytes == [0x01, 0xFE, 0xCB, 0xA9, 0x84]);
	assert(LongBytes == [0x01, 0xFE, 0xCB, 0xA9, 0x84, 0x01, 0x23, 0x45, 0xFF]);
	assert(StringBytes == ['S', 'o', 'u', 'p']);

	assert(BitConverter.To!bool(BoolByte, false) == false, format("Bytes = %s | Data = %s", BoolByte, BitConverter.To!bool(BoolByte, false)));
	assert(BitConverter.To!char(CharByte, false) == 's');
	assert(BitConverter.To!ushort(ShortBytes, true) == 0xFECB);
	assert(BitConverter.To!uint(IntBytes, true) == 0xFECBA984);
	assert(BitConverter.To!ulong(LongBytes, true) == 0xFECBA984012345FF);
	assert(BitConverter.ToString!string(StringBytes) == "Soup");
}