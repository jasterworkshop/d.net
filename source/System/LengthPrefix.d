﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.LengthPrefix;

enum LengthPrefix : ubyte
{
	None = 0,
	Byte = 1,
	Short = 2,
	Int = 4
}