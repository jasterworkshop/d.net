﻿module System.Collections.Stack;

private
{
}

public
{
	import System.ICloneable, System.Collections.IEnumerable, System.InvalidOperationException,
		System.Collections.GenericEnumerator;
}

class Stack(T) : ICloneable, IEnumerable!T
{
	private
	{
		/// The left-most side is the top of the stack, right-most is the bottom.
		T[]	_Stack;

		this(T[] clone)
		{
			this._Stack = clone;
		}
	}

	public
	{
		/// Construct a $(B System.Collections.Stack) object using $(B initialSize) as the initial size of the Stack.
		/// 
		/// Parameters:
		/// 	initialSize = The initial size of the stack.
		this(uint initialSize = 0)
		{
			this._Stack.length = initialSize;
		}

		/// Pushes the given object onto the top of the stack.
		/// 
		/// Parameters:
		/// 	object = The object to push onto the stack.
		void Push(T object)
		{
			this._Stack ~= object;
		}


		/// Pops the object that's on the top of the stack.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if the stack is empty.
		/// 
		/// Returns:
		/// 	The object on top of the stack.
		T Pop()
		{
			if(this._Stack.length == 0) throw new InvalidOperationException("Unable to pop an empty stack");
			T ToReturn = this._Stack[$ - 1];
			this._Stack.length -= 1;

			return ToReturn;
		}

		/// Creates a shallow copy of the current $(B Stack) object.
		/// 
		/// Returns:
		/// 	A clone of the current $(B Stack) object.
		Stack!T Clone()
		{
			return new Stack(this._Stack);
		}

		/// Returns an iterator that begins from the bottom of the stack
		GenericEnumerator!T GetEnumerator()
		{
			return new GenericEnumerator!T(this._Stack);
		}
	}
}
unittest
{
	auto Stak = new Stack!string(4);
}