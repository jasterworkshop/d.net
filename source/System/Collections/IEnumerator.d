﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.Collections.IEnumerator;

interface IEnumerator(T)
{
	/// Get the current $(B T) that the enumerator points to
	@property
	T Current();

	/// Move onto the next object.
	/// 
	/// Returns:
	/// 	$(B true) if the enumeartor successfully moved. $(B false) otherwise(For example, the entire Enumerable might have been iterated, so it can't move on anymore).
	bool MoveNext();

	/// Move the enumerator back it's original position
	void Reset();
}

