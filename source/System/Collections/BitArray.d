﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.Collections.BitArray;

public
{
	import System.ArgumentException, System.ArgumentOutOfRangeException,
		System.ICloneable, System.Collections.IEnumerable, 
		System.Collections.GenericEnumerator;
}

/// Manages an array of bytes and provides an interface to manipulate the bits of the bytes.
class BitArray : ICloneable, IEnumerable!ubyte
{
	private
	{
		ubyte[] _Data;

		void Operation(string Op)(BitArray array)
		{
			// Figure out which BitArray has the smaller size
			uint Length = (array.Bytes.length >= this._Data.length) ? this._Data.length : array.Bytes.length;
			
			for(uint i = 0; i < Length; i++)
			{
				static if(Op == "AND")
				{
					this._Data[i] &= array.Bytes[i];
				}
				static if(Op == "OR")
				{
					this._Data[i] |= array.Bytes[i];
				}
				static if(Op == "XOR")
				{
					this._Data[i] ^= array.Bytes[i];
				}
			}
		}
	}

	public
	{
		/// Constructs a BitArray with a specified number of bits.
		/// 
		/// Parameters:
		/// 	numberOfBits = The number of bits that the array should start off with. $(B Must be a multiple of 8)
		/// 
		/// 
		/// Throws $(B System.ArgumentException) if $(B numberOfBits) isn't a multiple of 8.
		this(uint numberOfBits)
		{
			this.Length = numberOfBits;
		}

		/// Sets the bit at the specified to index to 1 or 0 depending on the value of $(B value).
		/// 
		/// Parameters:
		/// 	bitIndex = The index of the bit to set
		/// 	value = The value to set the bit to. $(B true) sets the bit to $(B 1). $(B false) sets the bit to $(B 0)
		void Set(uint bitIndex, bool value)
		{
			// Figure out which byte holds the bit we want and where the bit will be in the byte.
			uint Byte = (bitIndex / 8);
			uint BitIndex = (bitIndex % 8);

			if(value == true)
			{
				this._Data[Byte] |= (0x01 << BitIndex);
			}
			else
			{
				this._Data[Byte] &= ~(0x01 << BitIndex);
			}
		}

		/// Sets all of the bits in the BitArray to $(B value)
		/// 
		/// Paramters:
		/// 	value = The value to set all the bits
		void SetAll(bool value)
		{
			this._Data[] = (value == true) ? 0xFF : 0x00;
		}

		/// Gets the value of the bit at $(B bitIndex), represented as a boolean
		/// 
		/// Parameters:
		/// 	bitIndex = The index of the bit to get the value of.
		/// 
		/// Returns:
		/// 	The value of the bit at $(B bitIndex). $(B true) means the bit is $(B 1). $(B false) means that the bit is $(B 0)
		bool Get(uint bitIndex)
		{
			uint Byte = (bitIndex / 8);
			uint BitIndex = (bitIndex % 8);

			return cast(bool)(this._Data[Byte] & (0x01 << BitIndex));
		}

		/// Makes a clone of the BitArray
		/// 
		/// Returns:
		/// 	A clone of the BitArray
		BitArray Clone()
		{
			auto ToReturn = new BitArray(this._Data.length * 8);
			ToReturn._Data = this._Data.dup;
			return ToReturn;
		}

		/// Performs a bitwise AND operation with the current BitArray's bytes and $(B toAndWith)'s bytes, the output of each operation is stored in the current BitArray.
		/// .
		/// Say the current BitArray had 4 bytes, and $(B toAndWith) had 8, only the first 4 bytes of the current BitArray would be ANDed by the first 4 bytes of $(B toAndWith).
		/// 
		/// Parameters:
		/// 	toAndWith = The BitArray to perform the AND operation on.
		alias Operation!("AND") And;

		/// Performs a bitwise OR operation with the current BitArray's bytes and $(B toOrWith)'s bytes, the output of each operation is stored in the current BitArray.
		/// .
		/// Say the current BitArray had 4 bytes, and $(B toOrWith) had 8, only the first 4 bytes of the current BitArray would be ORed by the first 4 bytes of $(B toOrWith).
		/// 
		/// Parameters:
		/// 	toOrWith = The BitArray to perform the OR operation on.
		alias Operation!("OR")  Or;
		alias Operation!("XOR") Xor;

		/// Returns the bytes that the BitArray is using
		@property
		ubyte[] Bytes()
		{
			return this._Data;
		}

		/// Sets the length of the BitArray in bits. $(numberOfBits) must be a multiple of 8
		@property
		void Length(uint numberOfBits)
		{
			if((numberOfBits % 8) != 0) throw new ArgumentOutOfRangeException("numberOfBits", "Must be a multiple of 8!");
			
			this._Data.length = (numberOfBits / 8);
		}

		/// Gets the length of the BitArray in bits.
		@property
		uint Length()
		{
			return (this._Data.length * 8);
		}

		/// Sets the bit at $(B index) to $(B value)
		void opIndexAssign(bool value, uint index)
		{
			this.Set(index, value);
		}

		/// Gets the value of the bit at $(B index)
		bool opIndex(uint index)
		{
			return this.Get(index);
		}

		/// Sets the value of the bits $(B index1..index2) to $(B value)
		void opSliceAssign(bool value, uint index1, uint index2)
		{
			foreach(bit; index1..index2)
			{
				this[bit] = value;
			}
		}

		/// Returns the values of the bits $(B index1..index2)
		bool[] opSlice(uint index1, uint index2)
		{
			bool[] ToReturn;
			foreach(bit; index1..index2)
			{
				ToReturn ~= this[bit];
			}

			return ToReturn;
		}

		/// Gets a $(B System.Collections.GenericEnumerator!ubyte) that iterates over every byte that this BitArray handles
		/// 
		/// Returns:
		/// 	An enumerator iterating over this BitArray's bytes
		GenericEnumerator!ubyte GetEnumerator()
		{
			return new GenericEnumerator!ubyte(this._Data);
		}

		void opOpAssign(string Op)(BitArray array)
		{
			static if(Op == "~")
			{
				this._Data ~= array._Data;
			}
			static if(Op == "&")
			{
				this.And(array);
			}
			static if(Op == "|")
			{
				this.Or(array);
			}
		}
	}
}
unittest
{
	auto Bits = new BitArray(8);

	Bits.Set(0, true); // 0x01
	Bits.Set(1, true); // 0x03
	Bits.Set(2, true); // 0x07
	Bits.Set(1, false); // 0x05

	assert(Bits.Bytes[0] == 0x05, format("0x%X", Bits.Bytes[0]));
	assert(Bits.Get(0) == true);
	assert(Bits.Get(1) == false);
	assert(Bits.Get(2) == true);

	Bits.SetAll(true);
	assert(Bits.Bytes[0] == 0xFF);

	Bits.SetAll(false);
	assert(Bits.Bytes[0] == 0x00);

	Bits[5] = true;
	assert(Bits[5] == true);

	Bits[0..5] = true;
	assert(Bits[2..4] == [true, true]);

	Bits.SetAll(true);
	auto Bits2 = Bits.Clone();
	assert(Bits2.Bytes[0] == 0xFF);

	Bits2.Bytes[0] = 0x1A;
	Bits2.And(Bits); // 0x1A
	assert(Bits2.Bytes[0] == 0x1A);

	Bits2.SetAll(false); // 0x00
	Bits2.Or(Bits); // 0xFF
	assert(Bits2.Bytes[0] == 0xFF);

	Bits2.SetAll(false); // 0x00
	Bits2 ~= Bits; // 0x00FF
	assert(Bits2.Bytes == [0x00, 0xFF]);

	Bits2.Length = 8; // 0x00
	Bits2 |= Bits; // 0xFF
	assert(Bits2.Bytes[0] == 0xFF);

	Bits.SetAll(false); // 0x00
	Bits.Bytes[0] = 0x1A;
	Bits2 &= Bits; // 0x1A
	assert(Bits2.Bytes[0] == 0x1A);

	Bits2.Length = 32;
	Bits2.Bytes[0] = 0xFF;
	Bits2.Bytes[1] = 0xFE;
	Bits2.Bytes[2] = 0xFD;
	Bits2.Bytes[3] = 0xFC;

	auto Iter = Bits2.GetEnumerator();
	ubyte Count = 0;

	do
	{
		assert(Iter.Current == cast(ubyte)(0xFF - Count));
		Count++;
	}while(Iter.MoveNext());
}