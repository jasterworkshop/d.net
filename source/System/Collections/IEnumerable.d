﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.Collections.IEnumerable;

public
{
	import System.Collections.IEnumerator;
}

interface IEnumerable(T)
{
	/// Gets an Enumerator that iterates throughout the Enumerable
	IEnumerator!T GetEnumerator();
}

