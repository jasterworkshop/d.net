﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.Collections.GenericEnumerator;

public
{
	import System.Collections.IEnumerator, System.ICloneable,
		System.ArgumentNullException;
}

class GenericEnumerator(T) : IEnumerator!T, ICloneable
{
	private
	{
		T[] 	_Data;
		uint 	_Index = 0;
	}

	public
	{
		/// Creates a generic enumerator that will iterate through $(B data).
		/// 
		/// Parameters:
		/// 	data = The data to iterate through
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B data) is null.
		this(T[] data)
		{
			if(data is null) throw new ArgumentNullException("data");

			this._Data = data;
		}

		/// Moves onto the next element.
		/// 
		/// Returns:
		/// 	$(B true) if the enumeartor successfully moved. $(B false) otherwise(For example, the entire Enumerable might have been iterated, so it can't move on anymore)
		bool MoveNext()
		{
			if((this._Index + 1) >= this._Data.length)
			{
				return false;
			}
			else
			{
				this._Index++;
				return true;
			}
		}

		/// Creates a clone of the Enumerator.
		/// 
		/// Returns:
		/// 	A clone of the enumerator
		GenericEnumerator!T Clone()
		{
			return new GenericEnumerator!T(this._Data);
		}

		/// Resets the position of the enumerator.
		void Reset()
		{
			this._Index = 0;
		}

		/// Returns the $(B T) that the enumerator currently points to.
		@property
		T Current()
		{
			return this._Data[this._Index];
		}
	}
}

