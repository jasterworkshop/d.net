﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.String;

private
{
	import std.string;
	import std.traits;

	import System.ArgumentNullException;
	import System.Char;
}

// These functions aren't in a class because it's a lot nice to use UFCS for these things

public
{
	/// Specifies whether $(B System.String.Split) methods include or omit empty substrings from the return value.
	enum StringSplitOptions : ubyte
	{
		/// The return value includes array elements that contain an empty string
		None = 0,

		/// The return value does not include array elements that contain an empty string
		RemoveEmptyEntries = 1
	}

	/// Concatenates all the elements of a string array, using the specified separator between each element.
	/// $(B seperator) can be null.
	/// 
	/// Returns:
	/// 	A string that consists of the elements in $(B values) delimited by the $(B seperator) string. If $(B values) is an empty array, the method returns an empty string.
	/// 
	/// Parameters:
	/// 	seperator = The string to use as a separator.
	/// 	value = An array that contains the elements to concatenate.
	/// 
	/// Throws $(B System.ArgumentNullException) if $(B values) is null.
	T Join(T)(in T seperator, in T[] values) 
	if(isSomeString!T)
	{
		// Making sure that values isn't null
		if(values is null)
		{
			throw new ArgumentNullException("values");
		}

		T ToReturn = "";

		foreach(i, value; values)
		{
			// This doesn't work properly Q_Q
			//(i == values.length - 1) ? ToReturn ~= value : ToReturn ~= (value ~ seperator);
			if(i == (values.length - 1) || seperator is null)
			{
				ToReturn ~= value;
			}
			else
			{
				ToReturn ~= (value ~ seperator);
			}
		}

		return ToReturn;
	}

	/// Indicates whether a specified string is null, empty, or consists only of white-space characters.
	/// 
	///	Parameters:
	/// 	value = The string to test.
	/// 
	/// Returns:
	/// 	$(D true) if the $(B value) parameter is $(D null) or an empty string, or if $(B value) consists exclusively of white-space characters.
	/// 
	bool IsNullOrWhiteSpace(T)(in T value)
	if(isSomeString!T)
	{
		if(value is null)
		{
			return true;
		}
		else
		{
			foreach(c; value)
			{
				if(!c.IsWhiteSpace())
				{
					return false;
				}
			}
		}

		return true;
	}

	/// Indicates whether the specified string is null or an empty string.
	/// 
	/// Parameters:
	///     value = The string to test.
	///     
	/// Returns:
	///     $(D true) if the $(B value) parameter is $(D null) or an empty string (""); otherwise, $(D false).
	/// 
	bool IsNullOrEmpty(T)(in T value)
	if(isSomeString!T)
	{
	    return (value is null || value.length == 0);
	}

	/// Removes all trailing and leading occurrences of a set of characters specified in $(B trimChars) from the string $(B value).
	/// 
	/// Parameters:
	/// 	trimChars = An array of Unicode characters to remove.
	/// 	value = The string to remove the trailing and leading characters from.
	/// 
	/// Throws $(B System.ArgumentNullException) if either paramter is $(D null)
	/// 
	/// Returns: 
	/// 	The string that remains after all occurrences of the characters in the $(B trimChars) parameter are removed from the start and end of the $(B value) string.
	T Trim(T)(T value, T trimChars)
	if(isSomeString!T)
	{
		return value.TrimStart(trimChars).TrimEnd(trimChars);
	}

	/// Removes all leading occurrences of a set of characters specified in $(B trimChars) from the string $(B value).
	/// 
	/// Parameters:
	/// 	trimChars = An array of Unicode characters to remove.
	/// 	value = The string to remove the leading characters from.
	/// 
	/// Throws $(B System.ArgumentNullException) if either paramter is $(D null)
	/// 
	/// Returns: 
	/// 	The string that remains after all occurrences of the characters in the $(B trimChars) parameter are removed from the start of the $(B value) string.
	T TrimStart(T)(T value, T trimChars)
	if(isSomeString!T)
	{
		if(value is null) throw new ArgumentNullException("value");
		else if(trimChars is null) throw new ArgumentNullException("trimChars");

		for(int i = 0; i < value.length; i++)
		{
			bool Contains = false;

			foreach(ch; trimChars)
			{
				if(value[i] == ch)
				{
					Contains = true;
					break;
				}
			}

			if(!Contains)
			{
				return value[i..$];
			}
		}

		return value;
	}

	/// Removes all trailing occurrences of a set of characters specified in $(B trimChars) from the string $(B value).
	/// 
	/// Parameters:
	/// 	trimChars = An array of Unicode characters to remove.
	/// 	value = The string to remove the trailing characters from.
	/// 
	/// Throws $(B System.ArgumentNullException) if either paramter is $(D null)
	/// 
	/// Returns: 
	/// 	The string that remains after all occurrences of the characters in the $(B trimChars) parameter are removed from the end of the $(B value) string.
	T TrimEnd(T)(T value, T trimChars)
	if(isSomeString!T)
	{
		if(value is null) throw new ArgumentNullException("value");
		else if(trimChars is null) throw new ArgumentNullException("trimChars");

		for(int i = value.length - 1; i > -1; i--)
		{
			bool Contains = false;

			foreach(ch; trimChars)
			{
				if(value[i] == ch)
				{
					Contains = true;
					break;
				}
			}

			if(!Contains)
			{
				return value[0..i + 1];
			}
		}

		return value;
	}

	/// Returns a string array that contains the substrings of $(B toSplit) that are delimited by $(B seperator).
	/// 
	/// Parameters:
	/// 	toSplit = String to split up.
	/// 	seperator = An array of Unicode characters that delimit the substrings in $(B toSplit), an empty array that contains no delimiters.
	/// 	options = Whether or not to remove elements in the return value that are simply whitespace
	/// 
	/// Throws $(B System.ArgumentNullException) if either $(B toSplit) or $(B seperator) is $(D null).
	/// 
	/// Returns:
	/// 	An array whose elements contain the substrings in $(B toSplit) that are delimited by $(B seperator).
	T[] Split(T)(T toSplit, T seperator, StringSplitOptions options = StringSplitOptions.None)
	if(isSomeString!T)
	{
		if(toSplit is null) throw new ArgumentNullException("toSplit");
		else if(seperator is null) throw new ArgumentNullException("seperator");

		T[] BufferThing = toSplit.split(seperator);

		if(options == StringSplitOptions.RemoveEmptyEntries)
		{
			T[] ToReturn = [];

			foreach(thing; BufferThing)
			{
				if(!thing.IsNullOrWhiteSpace())
				{
					ToReturn ~= thing;
				}
			}

			return ToReturn;
		}
		else
		{
			return BufferThing;
		}
	}

	/// Determines whether $(B toCheck) starts with $(B startsWith).
	/// 
	/// Parameters:
	/// 	toCheck = String to check the start of.
	/// 	startsWith = String to check for at the start of $(B toCheck).
	/// 
	/// Returns:
	/// 	True if $(B startsWith) is at the start of $(B toCheck), otherwise, false.
	bool StartsWith(T)(T toCheck, T startsWith)
	if(isSomeString!T)
	{
		if((toCheck is null || startsWith is null) || toCheck.length < startsWith.length)
		{
			return false;
		}
		else
		{
			return (toCheck[0..startsWith.length] == startsWith);
		}
	}

	/// Determines whether $(B toCheck) ends with $(B endsWith).
	/// 
	/// Parameters:
	/// 	toCheck = String to check the end of.
	/// 	endsWith = String to check for at the end of $(B toCheck).
	/// 
	/// Returns:
	/// 	True if $(B endsWith) is at the end of $(B toCheck), otherwise, false.
	bool EndsWith(T)(T toCheck, T endsWith)
	if(isSomeString!T)
	{
		if((toCheck is null || endsWith is null) || toCheck.length < endsWith.length)
		{
			return false;
		}
		else
		{
			return (toCheck[$ - endsWith.length..$] == endsWith);
		}
	}

	/// Converts any lower-case characters in $(B value) to their upper-case versions.
	/// 
	/// Parameters:
	/// 	value = The string to convert.
	/// 
	/// Returns:
	/// 	$(B value) in it's upper-case converted form.
	T ToUpper(T)(T value)
	if(isSomeString!T)
	{
		if(value is null) throw new ArgumentNullException("value");
		
		return value.toUpper();
	}

	/// Converts any upper-case characters in $(B value) to their lower-case versions.
	/// 
	/// Parameters:
	/// 	value = The string to convert.
	/// 
	/// Returns:
	/// 	$(B value) in it's lower-case converted form.
	T ToLower(T)(T value)
	if(isSomeString!T)
	{
		if(value is null) throw new ArgumentNullException("value");
		
		return value.toLower();
	}

	/// Determines whether $(B toCheck) contains $(B containString) at any point in it's data.
	/// 
	/// Parameters:
	/// 	toCheck = The string to check through for $(B containString).
	/// 	containString = The string to check for.
	/// 
	/// Returns:
	/// 	True if $(B toCheck) contains $(B containString), otherwise, false.
	bool Contains(T)(T toCheck, T containString)
	if(isSomeString!T)
	{
		if((toCheck is null || containString is null) || toCheck.length < containString.length) return false;

		for(int i = 0; i < toCheck.length; i++)
		{
			if(toCheck[i] == containString[0])
			{
				int Index = 1;
				while(true)
				{
					if(Index == containString.length)
					{
						return true;
					}

					if(i == toCheck.length - 1)
					{
						return false;
					}
					else if(toCheck[++i] != containString[Index++])
					{
						break;
					}
				}
			}
		}

		return false;
	}

	/// Replaces any occurance of $(B toReplace) with the contents of $(B toReplace) inside of $(input).
	/// 
	/// Parameters:
	/// 	input = The string to replace in
	/// 	toReplace = The string to look for, to replace
	/// 	toReplaceWith = The string to replace $(B toReplace) with
	/// 
	/// 
	/// Throws $(B System.ArgumentNullException) if any parameter is null.
	/// 
	/// Returns:
	/// 	$(B input) with all occurances of $(B toReplace) replaced with $(B toReplaceWith)
	T Replace(T)(T input, T toReplace, T toReplaceWith)
	if(isSomeString!T)
	{
		if(input is null) throw new ArgumentNullException("input");
		if(toReplace is null) throw new ArgumentNullException("toReplace");
		if(toReplaceWith is null) throw new ArgumentNullException("toReplaceWith");

		T Final = "";
		T Buffer = "";

		foreach(ch; input)
		{
			Buffer ~= ch;
			if(Buffer.length == toReplace.length)
			{
				if(Buffer == toReplace)
				{
					Final ~= toReplaceWith;
					Buffer = "";
					continue;
				}
				else
				{
					Final ~= Buffer;
					Buffer = "";
					continue;
				}
			}
			else
			{
				if(Buffer[$ - 1] != toReplaceWith[Buffer.length - 1])
				{
					Final ~= Buffer;
					Buffer = "";
					continue;
				}
			}
		}
		if(Buffer != null) Final ~= Buffer;

		return Final;
	}
}

//void Errors()
unittest
{
	string S = Join("-", ["Some", "String"]);
	wstring WS = Join(""w, ["Some"w, "String"w]);
	dstring DS = Join!dstring(null, ["Some"d, "String"d]);

	assert(S == "Some-String", S);
	assert(WS == "SomeString"w);
	assert(DS == "SomeString"d);

	assert(IsNullOrWhiteSpace(" ") && !S.IsNullOrWhiteSpace());
	assert(IsNullOrWhiteSpace!wstring(null) && !WS.IsNullOrWhiteSpace());
	assert(IsNullOrWhiteSpace("\r\t\n "d) && !DS.IsNullOrWhiteSpace());

    assert(IsNullOrEmpty("") && IsNullOrEmpty!string(null));

	assert(S.TrimStart("Some") == "-String");
	assert(WS.TrimEnd("ing"w) == "SomeStr"w);
	assert(DS.Trim("Someing"d) == "tr"d);

	assert(S.Split("-") == ["Some", "String"]);
	assert(Split("Hey- -There", "-", StringSplitOptions.RemoveEmptyEntries) == ["Hey", "There"]);

	assert(S.StartsWith("Some"));
	assert(WS.EndsWith("String"w));
	assert(DS.StartsWith("Som"d) && DS.EndsWith("ring"d));

	assert(S.ToUpper() == "SOME-STRING");
	assert(WS.ToLower() == "somestring"w);

	assert(S.Contains("-"));
	assert(WS.Contains("Some"w));
	assert(DS.Contains("String"d));

	assert(S.Replace("-", "_") == "Some_String");
	assert(WS.Replace("S"w, "D"w) == "DomeDtring"w);
}