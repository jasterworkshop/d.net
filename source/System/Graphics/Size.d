﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.Graphics.Size;

// TODO: Add the helper functions
struct Size
{
	/// The width of the size
	uint Width;

	/// The height of the size
	uint Height;
}

