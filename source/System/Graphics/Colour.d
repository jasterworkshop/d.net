﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.Graphics.Colour;

private
{
	import std.string : format;
}

// TODO: Helper functions
/// Contains data of a colour
struct Colour
{
	private
	{
		/// Tuples wouldn't work for some odd reason q_q
		/// [0] = R
		/// [1] = G
		/// [2] = B
		/// [3] = A
		ubyte[4] _RGBA;
	}

	public
	{
		/// Create a colour with the given values
		/// 
		/// Parameters:
		/// 	r = How red the colour is
		/// 	g = How green the colour is
		/// 	b = How blue the colour is
		/// 	a = How transparent the colour is
		this(ubyte r, ubyte g, ubyte b, ubyte a) nothrow
		{
			this([r, g, b, a]);
		}

		/// Create a colour from the given colour values in $(B rgba)
		/// 
		/// Parameters:
		/// 	rgba = The colour values to give this colour
		this(ubyte[4] rgba) nothrow
		{
			this._RGBA = rgba;
		}
	}

	// These lovely things get their own public block, just so I can fold the code away <3
	public
	{
		/// Get/Set how red the colour is
		@property
		void R(ubyte r) nothrow
		{
			this._RGBA[0] = r;
		}
		
		/// Ditto
		@property
		ubyte R() nothrow
		{
			return this._RGBA[0];
		}
		
		/// Get/Set how green the colour is
		@property
		void G(ubyte g) nothrow
		{
			this._RGBA[1] = g;
		}
		
		/// Ditto
		@property
		ubyte G() nothrow
		{
			return this._RGBA[1];
		}
		/// Get/Set how blue the colour is
		@property
		void B(ubyte b) nothrow
		{
			this._RGBA[2] = b;
		}
		
		/// Ditto
		@property
		ubyte B() nothrow
		{
			return this._RGBA[2];
		}
		
		/// Get/Set how transparent the colour is
		@property
		void A(ubyte a) nothrow
		{
			this._RGBA[3] = a;
		}
		
		/// Ditto
		@property
		ubyte A() nothrow
		{
			return this._RGBA[3];
		}

		/// Stores the RGBA value of the colour into an unsigned 32-bit integer.
		@property
		uint ToInt() nothrow
		{
			return (((this.R << (8 * 3)) | (this.G << (8 * 2)) | (this.B << 8)) | this.A);
		}

		/// Converts the Colour into a human readable format.
		string toString()
		{
			return format("R=%d, G=%d, B=%d, A=%d", this.R, this.G, this.B, this.A);
		}
	}

	// Likewise. TODO: Add more colours
	public static
	{
		Colour Red = Colour(255, 0, 0, 255);
		Colour Green = Colour(0, 255, 0, 255);
		Colour Blue = Colour(0, 0, 255, 255);
		Colour White = Colour(255, 255, 255, 255);
		Colour Black = Colour(0, 0, 0, 255);
	}
}
unittest
{
	import System.BitConverter;

	assert(Colour.Blue.ToInt == 0x0000FFFF);
	assert(Colour.White.ToInt == 0xFFFFFFFF);
	assert(Colour(0xAB, 0xCD, 0xEF, 0x00).ToInt == 0xABCDEF00);
	assert(Colour.Blue.toString() == "R=0, G=0, B=255, A=255");
	assert(BitConverter.GetBytes(Colour.Blue, false, ByteOrder.Little) == [0x00, 0x00, 0xFF, 0xFF]);
}