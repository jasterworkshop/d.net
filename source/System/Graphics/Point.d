﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.Graphics.Point;

// TODO: Add the helper functions
struct Point
{
	/// The X-Coordinate of the point
	uint X;

	/// The Y-Coordinate of the point
	uint Y;
}

