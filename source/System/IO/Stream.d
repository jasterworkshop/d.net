﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.Stream;

private
{
	import core.memory;

	import System.InvalidOperationException,
		System.ArgumentNullException, System.ArgumentOutOfRangeException,
		System.NotSupportedException;
}

public
{
	import System.IDisposable;
}

/// Provides the fields that represent reference points in streams for seeking.
enum SeekOrigin
{
	/// Specifies the beginning of a stream.
	Begin,

	/// Specifies the current position within a stream.
	Current,

	/// Specifies the end of a stream.
	End
}

/// Provides a generic view of a sequence of bytes.
abstract class Stream : IDisposable
{
	// TODO: Figure out how to do the async methods... TODO: Figure out how threading works
	private
	{
		void InternalCopyTo(Stream destination, int bufferSize)
		{
			ubyte[] Data;
			Data.length = bufferSize;

			int Count = 0;

			do
			{
				Count = this.Read(Data, 0, Data.length);
				destination.Write(Data, 0, Count);
			}
			while(Count != 0);

			GC.free(Data.ptr);
		}
	}

	public
	{
		// Properties
		abstract
		{
			/// When overridden in a derived class, gets a value indicating whether the current stream supports reading.
			/// 
			/// Returns:
			/// 	$(D true) if the stream supports reading; otherwise, $(D false).
			@property
			bool CanRead();

			/// When overridden in a derived class, gets a value indicating whether the current stream supports seeking.
			/// 
			/// Returns:
			/// 	$(D true) if the stream supports seeking; otherwise, $(D false).
			@property
			bool CanSeek();

			/// Gets a value that determines whether the current stream can time out.
			/// 
			/// Returns:
			/// 	A value that determines whether the current stream can time out.
			@property
			bool CanTimeout()
			{
				return false;
			}

			/// When overridden in a derived class, gets a value indicating whether the current stream supports writing.
			/// 
			/// Returns:
			/// 	$(D true) if the stream supports writing; otherwise, $(D false).
			@property
			bool CanWrite();

			/// When overridden in a derived class, gets the length in bytes of the stream.
			///
			/// Returns:
			/// 	A $(D long) value representing the length of the stream in bytes.
			@property
			ulong Length();

			/// When overridden in a derived class, gets or sets the position within the current stream.
			/// 
			/// Returns:
			/// 	The current position within the stream.
			@property
			ulong Position();

			/// ditto
			@property
			void Position(long position);

			/// Gets or sets a value, in miliseconds, that determines how long the stream will attempt to read before timing out.
			/// 
			/// Returns:
			/// 	A value, in miliseconds, that determines how long the stream will attempt to read before timing out.
			@property
			int ReadTimeout()
			{
				throw new InvalidOperationException("Timeouts not supported!");
			}

			/// Ditto
			@property
			void ReadTimeout(int timeout)
			{
				throw new InvalidOperationException("Timeouts not supported!");
			}

			/// Gets or sets a value, in miliseconds, that determines how long the stream will attempt to write before timing out.
			/// 
			/// Returns:
			/// 	A value, in miliseconds, that determines how long the stream will attempt to write before timing out.
			@property
			int WriteTimeout()
			{
				throw new InvalidOperationException("Timeouts not supported!");
			}

			/// Ditto
			@property
			void WriteTimeout(int timeout)
			{
				throw new InvalidOperationException("Timeouts not supported!");
			}
		}

		// Methods(Seperated just to be neat)
		abstract
		{
			/// Closes the current stream and releases any resources (such as sockets and file handles) associated with the current stream.
			void Close()
			{
				this.Dispose(true);
				this.destroy();
			}

			/// Releases the unmanaged resources used by the $(B Stream.IO.Stream) and optionally releases the managed resources.
			/// 
			/// Parameters:
			/// 	disposing = $(D true) to release both managed and unmanaged resources; $(D false) to release only unmanaged resources.</param>
			void Dispose(bool disposing);

			/// When overridden in a derived class, clears all buffers for this stream and causes any buffered data to be written to the underlying device.
			void Flush() { throw new NotSupportedException(); }

			/// When overridden in a derived class, reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
			/// 
			/// Parameters:
			/// 	buffer = An array of bytes. When this method returns, the buffer contains the specified byte array with the values between $(B offset) and ($(B offset) + $(B count) - 1) replaced by the bytes read from the current source.
			/// 	offset = The zero-based byte offset in $(B buffer) at which to begin storing the data read from the current stream.
			/// 	count = The maximum number of bytes to be read from the current stream.
			/// 
			/// Returns:
			/// 	The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.
			int Read(inout ubyte[] buffer, int offset, uint count);

			ubyte Peek();

			/// When overridden in a derived class, sets the position within the current stream.
			/// 
			/// Parameters:
			/// 	offset = A byte offset relative to the $(B origin) parameter.
			/// 	origin = A value of type $(B System.IO.SeekOrigin) indicating the reference point used to obtain the new position.
			/// 
			/// Returns:
			/// 	The new position within the current stream.
			long Seek(long offset, SeekOrigin origin);

			/// When overridden in a derived class, sets the length of the current stream.
			/// 
			/// Parameters:
			/// 	length = The desired length of the current stream in bytes.
			void SetLength(ulong length);

			/// When overridden in a derived class, writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
			/// 	buffer = An array of bytes. This method copies $(B count) bytes from $(B buffer) to the current stream.
			/// 	offset = The zero-based byte offset in $(B buffer) at which to begin copying bytes to the current stream.
			/// 	count = The number of bytes to be written to the current stream.
			void Write(const(ubyte)[] buffer, int offset, uint count);
		}

		/// Reads a byte from the stream and advances the position within the stream by one byte, or returns -1 if at the end of the stream.
		/// 
		/// Returns:
		/// 	The unsigned byte cast to an Int32, or -1 if at the end of the stream.
		int ReadByte()
		{
			ubyte[] ToReturn = [0];
			
			if(this.Read(ToReturn, 0, 1) == 0)
			{
				return -1;
			}
			
			return cast(int)ToReturn[0];
		}

		/// Writes a byte to the current position in the stream and advances the position within the stream by one byte.
		/// 
		/// Parameters:
		/// 	data = The byte to write to the stream.
		void WriteByte(ubyte data)
		{
			this.Write([data], 0, 1);
		}

		/// Reads the bytes from the current stream and writes them to the destination stream.
		/// 
		/// Parameters:
		/// 	destination = The stream that will contain the contents of the current stream.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B destination) is null.
		void CopyTo(Stream destination)
		{
			if(destination is null) throw new ArgumentNullException("destination");

			this.InternalCopyTo(destination, 81920);
		}

		/// Reads the bytes from the current stream and writes them to the destination stream, using a specified buffer size.
		/// 
		/// Parameters:
		/// 	destination = The stream that will contain the contents of the current stream.
		/// 	bufferSize = The size of the buffer. This value must be greater than zero. The default size is 4096.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B destination) is null.
		/// Throws $(B System.ArgumentOutOfRangeException) if $(B bufferSize) is negative or 0.
		void CopyTo(Stream destination, int bufferSize = 4096)
		{
			if(destination is null) throw new ArgumentNullException("destination");
			else if(bufferSize <= 0) throw new ArgumentOutOfRangeException("bufferSize", "A positive number is needed");

			this.InternalCopyTo(destination, bufferSize);
		}

		/// Releases all resources used by the $(B System.IO.Stream).
		void Dispose()
		{
			this.Close();
		}
	}
}

