﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.BinaryReader;

private
{
	import std.traits;
	
	import System.BitConverter;
}

public
{
	import System.IO.Stream, System.ByteOrder,
		System.LengthPrefix, System.ArgumentException, System.ArgumentNullException, 
		System.IDisposable, System.IO.IOException, System.InvalidOperationException;
}

class BinaryReader : IDisposable
{
	private
	{
		Stream		_Stream;
		bool		_LeaveOpen;
		bool		_UseBOM;
		ByteOrder	_Endianness;
	}
	
	public
	{
		/// Constructs a BinaryReader that will close it's stream when $(B Close) or $(B Dipose) is called, and will expect to read in a Byte-order-mark when reading.
		/// 
		/// Parameters:
		/// 	stream = The stream to wrap the BinaryReader around
		this(Stream stream)
		{
			this(stream, false, true);
		}

		/// Constructs a BinaryReader that will close it's stream when $(B Close) or $(B Dipose) is called if $(B leaveOpen) is $(B false), and will expect to read in a Byte-order-mark when reading.
		/// 
		/// Parameters:
		/// 	stream = The stream to wrap the BinaryReader around
		/// 	leaveOpen = leaveOpen = $(B true) if the BinaryReader should simply flush $(B stream) when $(B Dispose) or $(B Close) is called, or $(B false) if the stream should be closed when either function is called.
		this(Stream stream, bool leaveOpen)
		{
			this(stream, leaveOpen, true);
		}

		/// Constructs a BinaryReader that will close it's stream when $(B Close) or $(B Dipose) is called if $(B leaveOpen) is $(B false), and will expect to read in a Byte-order-mark when reading if $(B usesBOM) is $(B true).
		/// 
		/// Parameters:
		/// 	stream = The stream to wrap the BinaryReader around
		/// 	leaveOpen = leaveOpen = $(B true) if the BinaryReader should simply flush $(B stream) when $(B Dispose) or $(B Close) is called, or $(B false) if the stream should be closed when either function is called.
		/// 	usesBOM = $(B true) if the BinaryReader expects to read in a byte denoting the ByteOrder of the data it tries to read in. $(B false) otherwise
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(B System.InvalidOperationException) if $(B stream.CanRead) is false.
		this(Stream stream, bool leaveOpen, bool usesBOM)
		{
			if(stream is null) throw new ArgumentNullException("stream");
			if(!stream.CanRead) throw new InvalidOperationException("Stream must be able to read!");
			
			this._Stream = stream;
			this._LeaveOpen = leaveOpen;
			this._UseBOM = usesBOM;
		}

		/// Possibly close the BinaryReader's stream
		void Close()
		{
			this.Dispose(true);
		}

		/// Ditto
		void Dispose()
		{
			this.Dispose(true);
		}

		/// Seeks to the $(B offset) from $(B origin) in the BinaryReader's stream
		/// 
		/// Parameters:
		/// 	offset = The offset to apply to $(B origin)
		/// 	origin = The origin point to apply $(B offset)
		/// 
		/// Returns:
		/// 	The BinaryReader's stream's new position
		long Seek(int offset, SeekOrigin origin)
		{
			return this._Stream.Seek(offset, origin);
		}

		/// Attempts to read in enough bytes to create an object of $(B T).
		/// 
		/// Returns:
		/// 	A $(B T) object.
		T ReadT(T)()
		{
			return BitConverter.To!T(this.ReadBytes(T.sizeof), false);
		}

		/// Reads 1 | 2 byte(s) and returns it as a boolean.
		/// 
		/// Returns:
		/// 	A boolean
		bool ReadBool() { return cast(bool)this._Stream.ReadByte(); }
		/// Reads 1 | 2 byte(s) and returns it as a character.
		/// 
		/// Returns:
		/// 	A character
		char ReadChar() { return cast(char)this._Stream.ReadByte(); }
		/// Reads 1 | 2 byte(s) and returns it as an unsigned byte.
		/// 
		/// Returns:
		/// 	An unsigned byte
		ubyte ReadByte() { return cast(ubyte)this._Stream.ReadByte(); }
		/// Reads count | count + 1 bytes and returns them in an array.
		/// 
		/// Returns:
		/// 	An unsigned byte array
		ubyte[] ReadBytes(uint count) 
		{
			ubyte[] Buffer; 
			Buffer.length = count + 1; // count + 1, because we're gonna have a BOM no matter what

			// Put a BOM at the start manually if we're not reading in one
			if(!this.UsesBOM) Buffer[0] = cast(ubyte)this._Endianness;

			// If we're reading in a BOM, read from the start. If we're putting a BOM in front manually, read from 1 byte after it.
			// Read (count + 1) bytes if we're reading in a BOM. Otherwise, read in count bytes
			this._Stream.Read(Buffer, cast(uint)(!this.UsesBOM), (this.UsesBOM) ? Buffer.length : count); 
			return BitConverter.ToArray!ubyte(Buffer, true); // This sorts out the ByteOrder
		}

		/// Reads 2 | 3 byte(s) and returns it as a word character.
		/// 
		/// Returns:
		/// 	A word character
		alias ReadChar16 = ReadT!wchar;
		/// Reads 4 | 5 byte(s) and returns it as a double word character.
		/// 
		/// Returns:
		/// 	A double word character
		alias ReadChar32 = ReadT!dchar;
		/// Reads 2 | 3 byte(s) and returns it as a 16-bit signed integer.
		/// 
		/// Returns:
		/// 	A 16-bit signed integer.
		alias ReadInt16 = ReadT!short;
		/// Reads 4 | 5 byte(s) and returns it as a 32-bit signed integer.
		/// 
		/// Returns:
		/// 	A 32-bit signed integer.
		alias ReadInt32 = ReadT!int;
		/// Reads 8 | 9 byte(s) and returns it as a 64-bit signed integer.
		/// 
		/// Returns:
		/// 	A 64-bit signed integer.
		alias ReadInt64 = ReadT!long;
		/// Reads 2 | 3 byte(s) and returns it as a 16-bit unsigned integer.
		/// 
		/// Returns:
		/// 	A 16-bit unsigned integer.
		alias ReadUInt16 = ReadT!ushort;
		/// Reads 4 | 5 byte(s) and returns it as a 32-bit unsigned integer.
		/// 
		/// Returns:
		/// 	A 32-bit unsigned integer.
		alias ReadUInt32 = ReadT!uint;
		/// Reads 8 | 9 byte(s) and returns it as a 64-bit unsigned integer.
		/// 
		/// Returns:
		/// 	A 64-bit unsigned integer.
		alias ReadUInt64 = ReadT!ulong;
		/// Reads 4 | 5 byte(s) and returns it as a single-precision floating point number.
		/// 
		/// Returns:
		/// 	A single-precision floating point number.
		alias ReadFloat = ReadT!float;
		/// Reads 8 | 9 byte(s) and returns it as a double-precision floating point number.
		/// 
		/// Returns:
		/// 	A double-precision floating point number.
		alias ReadDouble = ReadT!double;

		int Read7BitEncodedInt()
		{
			int num = 0;
			int num2 = 0;
			while (num2 != 35)
			{
				byte b = this.ReadByte();
				num |= cast(int)(b & 127) << num2;
				num2 += 7;
				if ((b & 128) == 0)
				{
					return num;
				}
			}
			throw new IOException("Invalid 7Bit Int!");
		}

		/// Reads in a length-prefixed string. However, if the length-prefix specified is $(B System.IO.LengthPrefix.None) then $(B characterCount) amount of characters are read.
		/// 
		/// Parameters:
		/// 	characterCount = How many characters to read in if the length-prefix of the string to read in is $(B System.IO.LengthPrefix.None)
		/// 
		/// 
		/// Throws $(B System.IO.IOException) if an invalid length-prefix identifier is read in.
		/// Throws $(B System.InvalidOperationException) if the function's internal buffer is somehow null.
		/// 
		/// Returns:
		/// 	A string of type T
		T ReadString(T)(lazy uint characterCount = 0)
		if(isSomeString!T)
		{
			LengthPrefix PrefixLength = cast(LengthPrefix)this.BaseStream.ReadByte();
			ubyte[] Buffer;

			switch(PrefixLength)
			{
				case LengthPrefix.Byte: Buffer.length = (this.ReadByte() * PrefixLength); break;
				case LengthPrefix.Short: Buffer.length = (this.ReadUInt16() * PrefixLength); break;
				case LengthPrefix.Int: Buffer.length = (this.ReadUInt32() * PrefixLength); break;
					
				case LengthPrefix.None:
					Buffer.length = characterCount;
					break;

				default:
					throw new IOException(std.string.format("Invalid prefix-identifier: ", PrefixLength));
			}
			if(Buffer is null) throw new InvalidOperationException("Unknown error while reading string. Buffer is null");

			this._Stream.Read(Buffer, 0, Buffer.length);
			return BitConverter.ToString!T(Buffer);
		}

		/// Gets the stream that the BinaryReader is using
		@property
		Stream BaseStream()
		{
			return this._Stream;
		}

		/// Gets/Sets whether the BinaryReader expects that the data it reads in will start with a byte denoting it's byte order.
		/// If this is true, then the reader will read in (SizeOfData + 1) amount of bytes
		@property
		bool UsesBOM()
		{
			return this._UseBOM;
		}

		/// Ditto
		@property
		void UsesBOM(bool bom)
		{
			this._UseBOM = bom;
		}

		/// Set the $(B System.ByteOrder) the BinaryReader should use if $(B BinaryReader.UsesBOM) is false.
		@property
		void Order(ByteOrder byteOrder)
		{
			this._Endianness = byteOrder;
		}

		/// Get the $(B System.ByteOrder) the BinaryReader should use if $(B BinaryReader.UsesBOM) is false.
		@property
		ByteOrder Order()
		{
			return this._Endianness;
		}
	}
	
	protected
	{
		void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(!this._LeaveOpen)
				{
					this._Stream.Close();
				}
			}
		}
	}
}
unittest
{
	// The unittest for this class is in BinaryWriter's unittest
}