﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.BinaryWriter;

private
{
	import std.traits;

	import System.BitConverter;
}

public
{
	import System.IO.Stream, System.LengthPrefix,
		System.ByteOrder, System.ArgumentException, System.ArgumentNullException, 
		System.IDisposable, System.InvalidOperationException;
}

class BinaryWriter : IDisposable
{
	private
	{
		Stream		_Stream;
		ByteOrder	_Order;

		bool		_LeaveOpen;
		bool		_UseBOM;
	}

	public
	{
		/// Constructs a Binary Writer that will write out data in the machine's native endianness, will close instead of flush the stream once $(B Close) or $(B Dispose) is called, and will write out byte-order-marks with it's data.
		/// 
		/// Parameters:
		/// 	stream = The Stream to wrap the BinaryWriter around.
		this(Stream stream)
		{
			this(stream, NativeByteOrder, false, true);
		}

		/// Constructs a Binary Writer that will write out data in $(B order) byte order, will close instead of flush the stream once $(B Close) or $(B Dispose) is called, and will write out byte-order-marks with it's data.
		/// 
		/// Parameters:
		/// 	stream = The Stream to wrap the BinaryWriter around.
		/// 	order = The byte order that the BinaryWriter will use when writing out it's data.
		this(Stream stream, ByteOrder order)
		{
			this(stream, order, false, true);
		}

		/// Constructs a Binary Writer that will write out data in $(B order) byte order, will close instead of flush the stream once $(B Close) or $(B Dispose) is called if $(B leaveOpen) is $(B false), and will write out byte-order-marks with it's data.
		/// 
		/// Parameters:
		/// 	stream = The Stream to wrap the BinaryWriter around.
		/// 	order = The byte order that the BinaryWriter will use when writing out it's data.
		/// 	leaveOpen = $(B true) if the BinaryWriter should simply flush $(B stream) when $(B Dispose) or $(B Close) is called, or $(B false) if the stream should be closed when either function is called.
		this(Stream stream, ByteOrder order, bool leaveOpen)
		{
			this(stream, order, leaveOpen, true);
		}


		/// Constructs a Binary Writer that will write out data in $(B order) byte order, will close instead of flush the stream once $(B Close) or $(B Dispose) is called if $(B leaveOpen) is $(B false), and will write out byte-order-marks with it's data if $(B usesBOM) is $(B true).
		////
		/// Parameters:
		/// 	stream = The Stream to wrap the BinaryWriter around.
		/// 	order = The byte order that the BinaryWriter will use when writing out it's data.
		/// 	leaveOpen = $(B true) if the BinaryWriter should simply flush $(B stream) when $(B Dispose) or $(B Close) is called, or $(B false) if the stream should be closed when either function is called.
		///		usesBOM = $(B true) if the BinaryWriter should write a single byte denoting a piece of data's byte order when it is written, or $(B false) to not write out this byte
		///
		/// Throws $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(B System.InvalidOperationException) if $(B stream.CanWrite) is false.
		this(Stream stream, ByteOrder order, bool leaveOpen, bool usesBOM)
		{
			if(stream is null) throw new ArgumentNullException("stream");
			if(!stream.CanWrite) throw new InvalidOperationException("Stream must be able to write!");

			this._Stream = stream;
			this._Order = order;
			this._LeaveOpen = leaveOpen;
			this._UseBOM = usesBOM;
		}

		/// Closes or flushes the BinaryWriter's stream.
		void Close()
		{
			this.Dispose(true);
		}

		/// ditto
		void Dispose()
		{
			this.Dispose(true);
		}

		/// Flushes the BinaryWriter's stream
		void Flush()
		{
			this._Stream.Flush();
		}

		/// Seeks to the $(B offset) from $(B origin) in the BinaryWriter's stream
		/// 
		/// Parameters:
		/// 	offset = The offset to apply to $(B origin)
		/// 	origin = The origin point to apply $(B offset)
		/// 
		/// Returns:
		/// 	The BinaryWriter's stream's new position
		long Seek(int offset, SeekOrigin origin)
		{
			return this._Stream.Seek(offset, origin);
		}

		/// Attempts to read in enough bytes to create an object of $(B T).
		/// 
		/// Returns:
		/// 	A $(B T) object.
		void WriteT(T)(T data)
		{
			ubyte[] Buffer = BitConverter.GetBytes(data, this.UsesBOM, this._Order); 
			this._Stream.Write(Buffer, 0, Buffer.length);
		}

		/// Writes the boolean $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The boolean value to write.
		void Write(bool value) { this._Stream.WriteByte(cast(ubyte)value); }
		/// Writes the character $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The character value to write.
		void Write(char value) { this._Stream.WriteByte(cast(ubyte)value); }
		/// Writes the unsigned byte $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The unsigned byte value to write.
		void Write(ubyte value) { this._Stream.WriteByte(cast(ubyte)value); }
		/// Writes the unsigned byte array $(B buffer) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	buffer = The unsigned byte array to write.
		void Write(ubyte[] buffer) { this._Stream.Write(BitConverter.GetBytes(buffer, this._UseBOM, this._Order), 0, this._UseBOM ? buffer.length + 1 : buffer.length); }
		/// Writes $(count) amount of byte from $(B offset) in $(B buffer) into the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	buffer = The unsigned byte array that contains the data to write.
		/// 	offset = The position of where the data to write starts in $(B buffer).
		/// 	count = How many bytes from $(B buffer) to write.
		void Write(ubyte[] buffer, int offset, int count) { this._Stream.Write(BitConverter.GetBytes(buffer, this._UseBOM, this._Order), offset, count); }
		/// Writes the word character $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The word character value to write.
		void Write(wchar value) { ubyte[] Buffer = BitConverter.GetBytes(value, this.UsesBOM, this._Order); this._Stream.Write(Buffer, 0, Buffer.length); }
		/// Writes the double word character $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The double word character value to write.
		void Write(dchar value) { ubyte[] Buffer = BitConverter.GetBytes(value, this.UsesBOM, this._Order); this._Stream.Write(Buffer, 0, Buffer.length); }
		/// Writes the 16-bit signed integer $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The 16-bit signed integer value to write.
		void Write(short value) { ubyte[] Buffer = BitConverter.GetBytes(value, this.UsesBOM, this._Order); this._Stream.Write(Buffer, 0, Buffer.length); }
		/// Writes the 32-bit signed integer $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The 32-bit signed integer value to write.
		void Write(int value) { ubyte[] Buffer = BitConverter.GetBytes(value, this.UsesBOM, this._Order); this._Stream.Write(Buffer, 0, Buffer.length); }
		/// Writes the 64-bit signed integer $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The 64-bit signed integer value to write.
		void Write(long value) { ubyte[] Buffer = BitConverter.GetBytes(value, this.UsesBOM, this._Order); this._Stream.Write(Buffer, 0, Buffer.length); }
		/// Writes the 16-bit unsigned integer $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The 16-bit unsigned integer value to write.
		void Write(ushort value) { ubyte[] Buffer = BitConverter.GetBytes(value, this.UsesBOM, this._Order); this._Stream.Write(Buffer, 0, Buffer.length); }
		/// Writes the 32-bit unsigned integer $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The 32-bit unsigned integer value to write.
		void Write(uint value) { ubyte[] Buffer = BitConverter.GetBytes(value, this.UsesBOM, this._Order); this._Stream.Write(Buffer, 0, Buffer.length); }
		/// Writes the 64-bit unsigned integer $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The 64-bit unsigned integer value to write.
		void Write(ulong value) { ubyte[] Buffer = BitConverter.GetBytes(value, this.UsesBOM, this._Order); this._Stream.Write(Buffer, 0, Buffer.length); }
		/// Writes the single-precision floating point number $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The single-precision floating point number value to write.
		void Write(float value) { ubyte[] Buffer = BitConverter.GetBytes(value, this.UsesBOM, this._Order); this._Stream.Write(Buffer, 0, Buffer.length); }
		/// Writes the double-precision floating point number $(B value) to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The double-precision floating point number value to write.
		void Write(double value) { ubyte[] Buffer = BitConverter.GetBytes(value, this.UsesBOM, this._Order); this._Stream.Write(Buffer, 0, Buffer.length); }

		/// Writes the 32-bit signed integer $(B value) to the BinaryWriter's stream in a compressed format.
		/// 
		/// Parameters:
		/// 	value = The 32-bit signed integer value to compress and write.
		void Write7BitEncodedInt(int value)
		{
			uint num = 0;
			for (num = cast(uint)value; num >= 128u; num >>= 7)
			{
				this.Write(cast(ubyte)(num | 128u));
			}
			this.Write(cast(ubyte)num);
		}

		/// Turns the structure $(B value) into it's byte form, and then writes it to the BinaryWriter's stream.
		/// 
		/// Parameters:
		/// 	value = The stucture to write.
		void Write(S)(S value)
		if(is(S == struct))
		{
			ubyte[] Buffer = BitConverter.AttemptGetBytes(value, this._Order);
			this._Stream.Write(Buffer, this._UseBOM ? 0 : 1, this._UseBOM ? Buffer.length : Buffer.length - 1);
		}

		/// Writes a length-prefixed string to the BinaryWriter's stream.
		/// The amount of bytes that the length-prefix will take up is determined by $(B prefixLength).
		/// The length-prefix's value only specifies how many characters make up the string, not how many bytes the entire string takes up
		/// 
		/// Parameters:
		/// 	value = The string to write.
		/// 	prefixLength = How large in bytes the length-prefix will be, the bigger the prefixLength, the more characters that the string can hold.
		void Write(T)(T value, LengthPrefix prefixLength = LengthPrefix.Short)
		if(isSomeString!T)
		{
			ubyte[] Buffer;
			Buffer ~= cast(ubyte)prefixLength;

			switch(prefixLength)
			{
				// I'm not sure which I hate more, these cases taking up needlessly large amounts of space by having them span multiple lines, or having them like this...
				case LengthPrefix.Byte: if(value.length > ubyte.max) throw new ArgumentException("value", "Too many characters for the given prefixLength!"); else Buffer ~= cast(ubyte)value.length; break;
				case LengthPrefix.Short: if(value.length > ushort.max) throw new ArgumentException("value", "Too many characters for the given prefixLength!"); else Buffer ~= BitConverter.GetBytes(cast(ushort)value.length, this.UsesBOM, ByteOrder.Big); break;
				case LengthPrefix.Int: if(value.length > uint.max) throw new ArgumentException("value", "Too many characters for the given prefixLength!"); else Buffer ~= BitConverter.GetBytes(cast(uint)value.length, this.UsesBOM, ByteOrder.Big); break;

				case LengthPrefix.None:
				default:
					break;
			}

			Buffer ~= BitConverter.GetBytes!T(value);
			this._Stream.Write(Buffer, 0, Buffer.length);
		}

		/// Returns the stream that the BinaryWriter is using.
		@property
		Stream BaseStream()
		{
			return this._Stream;
		}

		/// Gets/Sets whether the BinaryWriter should write out a byte-order-mark with it's data.
		@property
		bool UsesBOM()
		{
			return this._UseBOM;
		}

		/// Ditto
		@property
		void UsesBOM(bool bom)
		{
			this._UseBOM = bom;
		}

		/// Gets/Sets the ByteOrder of the BinaryWriter.
		@property
		ByteOrder Order()
		{
			return this._Order;
		}

		/// Ditto
		@property
		void Order(ByteOrder order)
		{
			this._Order = order;
		}
	}

	protected
	{
		void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(this._LeaveOpen)
				{
					this._Stream.Flush();
				}
				else
				{
					this._Stream.Close();
				}
			}
		}
	}
}
//void ErrorsPles()
unittest
{
	import std.string : format;
	import System.IO.FileStream, System.IO.BinaryReader;

	struct Test
	{
		uint Int;
		char Gurl;
	}
	Test TS = Test(0xFFEEBBCC, 'S');

	FileStream FS = new FileStream("unittests/BWRTest.bin", FileMode.Create, FileAccess.ReadWrite);
	BinaryWriter BW = new BinaryWriter(FS, ByteOrder.Big, true);
	BW.Write(cast(ushort)0xFEDC);
	BW.Write("Soup", LengthPrefix.Byte);
	BW.Write("Soup"w, LengthPrefix.Short);
	BW.Write("Soup"d, LengthPrefix.Int);
	BW.WriteT!Test(TS);
	BW.UsesBOM = false;
	BW.Write(cast(uint)0xFEDCBA98);

	BW.BaseStream.Position = 0;
	BW.Dispose();

	version(BigEndian)
	{
		uint Data = 0xFEDCBA98;
	}
	else
	{
		uint Data = 0x98BADCFE;
	}

	BinaryReader BR = new BinaryReader(FS);
	ushort Daeth = BR.ReadUInt16();
	assert(Daeth == cast(ushort)0xFEDC, std.string.format("0x%X", Daeth)); 
	assert(BR.ReadString!string() == "Soup");
	assert(BR.ReadString!wstring() == "Soup");
	assert(BR.ReadString!dstring() == "Soup");
	assert(BR.ReadT!Test() == TS);
	BR.UsesBOM = false;
	assert(BR.ReadUInt32() == Data);
	BR.Dispose();
}