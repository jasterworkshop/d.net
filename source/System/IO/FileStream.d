﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.FileStream;

private
{
	import std.stdio : File, fread, SEEK_SET, SEEK_CUR, SEEK_END;
	import std.file : remove, exists, write;
	import std.path : absolutePath;

	version(unittest)
	{
		import std.stdio : writeln, readln;
	}

	import System.ArgumentNullException, System.ArgumentException,
		System.IO.IOException, System.IO.FileNotFoundException,
		System.NotSupportedException, System.ArgumentOutOfRangeException,
		System.IO.Path;
}

public
{
	import System.IO.FileAccess, System.IO.FileMode,
		System.IO.Stream;
}

class FileStream : Stream
{
	// TODO: Lock functions
	private
	{
		FileAccess 	_Access;
		FileMode	_Mode;

		File		_File;
	}

	public
	{
		/// Constructs a file stream for the file at $(B filePath) using $(B System.IO.FileMode.OpenOrCreate) and $(B System.IO.FileAccess.ReadWrite)
		/// 
		/// Parameters:
		/// 	filePath = File the construct the stream around
		this(string filePath)
		{
			this(filePath, FileMode.OpenOrCreate);
		}

		/// Constructs a file stream for the file at $(B filePath) using $(B mode) and $(B System.IO.FileAccess.ReadWrite)
		/// 
		/// Parameters:
		/// 	filePath = File the construct the stream around.
		/// 	mode = How to handle the file at $(B filePath).
		this(string filePath, FileMode mode)
		{
			this(filePath, mode, FileAccess.ReadWrite);
		}

		/// Constructs a file stream for the file at $(B filePath) using $(B mode) and $(B access).
		/// 
		/// Parameters:
		/// 	filePath = File the construct the stream around.
		/// 	mode = How to handle the file at $(B filePath).
		/// 	access = What type of operations the Stream has access to.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B filePath) is null.
		/// Throws $(B System.ArgumentException) if $(B mode) is $(B System.IO.FileMode.Append) and $(B access) doesn't grant access for the stream to write.
		/// Throws $(B System.IOException) if $(B mode) is $(B System.IO.FileMode.CreateNew) and a file already exists at $(B filePath) 
		/// Throws $(B System.IOException) if the stream was unable to open the file at $(B filePath)
		this(string filePath, FileMode mode, FileAccess access)
		{
			// Make sure the filePath isn't null, as we need a valid path
			if(filePath is null) throw new ArgumentNullException("filePath");

			this._Mode = mode;
			this._Access = access;

			string OpenMode = "r"; // It will always open in at least "rb" just so the file doesn't get overriden from having a "wb" open mode
			string FilePath = Path.ResolvePath(filePath);

			// Add in the "+" if writing is needed, and the "b" since we're always gonna open the file in binary mode
			if(this.CanWrite) OpenMode ~= "+";
			OpenMode ~= "b";

			// Make sure we can write with FileMode.Append, as this is how the .Net FileStream works.
			if(mode == FileMode.Append && !this.CanWrite) throw new ArgumentException("mode", "FileMode.Append can not be used with FileAccess.Read.");

			// Creates a new file, throwing an exception if filePath already has a file
			void CreateNew()
			{
				if(FilePath.exists())
				{
					throw new IOException("File already exists: " ~ FilePath);
				}
				else
				{
					write(FilePath, null);
					this._File.open(FilePath, OpenMode);
				}
			}

			// Opens a file, throwing an exception if no file exists
			void Open()
			{
				if(FilePath.exists())
				{
					this._File.open(FilePath, OpenMode);
				}
				else
				{
					throw new FileNotFoundException(FilePath);
				}
			}

			// Creates an empty file no matter what
			void Create()
			{
				if(FilePath.exists())
				{
					FilePath.remove();
					CreateNew();
				}
				else
				{
					CreateNew();
					Open();
				}
			}

			// Opens an existing file, or creates an empty one.
			void OpenOrCreate()
			{
				if(!FilePath.exists())
				{
					CreateNew();
				}
				else
				{
					Open();
				}
			}

			// Opens the file, goes to the end of the file, and prevents seeking and reading functions from working.
			void Append()
			{
				OpenOrCreate();
				this.Seek(0, SeekOrigin.End);
			}

			if(mode == FileMode.Create) Create();
			else if(mode == FileMode.Append) Append();
			else if(mode == FileMode.CreateNew) CreateNew();
			else if(mode == FileMode.Open) Open();
			else if(mode == FileMode.OpenOrCreate) OpenOrCreate();

			if(!this._File.isOpen) throw new IOException("Unable to open file: " ~ FilePath);
		}

		/// Gets the name of the file that the stream is working with.
		@property
		string Name()
		{
			return this._File.name;
		}

		/// Calls the normal $(B Write) function with the given parameters and then proceeds to flush the stream.
		/// .
		/// $(B Requires that the stream was opened with access to write to the file)
		/// 
		/// 
		/// Parameters:
		/// 	buffer = Byte array that contains the data to write
		/// 	offset = Offset to where the data to write begins in $(D buffer)
		/// 	count = How many bytes from $(B buffer[offset]) to write into the file
		void WriteFlush(ubyte[] buffer, int offset, uint count)
		{
			this.Write(buffer, offset, count);
			this.Flush();
		}

		/// Gets whether the stream is at the end of the file
		@property
		bool EOF()
		{
			return (this.Position >= this.Length);
		}

		override
		{
			// Maybe I shouldn't use this.destroy()... TODO: Make an "IsOpen" flag, and use that in every function to make sure we can do things.

			/// Closes the stream and calls $(B Dispose)
			///.
			///	 $(B NOTE) that this function calls the $(B destroy) function on the FileStream object, leaving it in an invalid state. Do not use the object after calling this function.
			void Close()
			{
				this.Dispose(true);
			}

			/// If $(B disposing) is true, flushes the Stream, closes access to the file, and then destroys the FileStream.
			/// .
			/// $(B NOTE) that this function calls the $(B destroy) function on the FileStream object, leaving it in an invalid state. Do not use the object after calling this function.
			/// 
			/// Parameters:
			/// 	disposing = Should be true, no functionality has been added for if this is false.
			void Dispose(bool disposing)
			{
				if(disposing)
				{
					this._File.flush();
					this._File.close();
					this.destroy();
				}
			}

			/// Flushes the stream, writing it's buffer to the file and then clearing it.
			void Flush()
			{
				this._File.flush();
			}

			/// Reads $(B count) amount of bytes from the file and stores it in $(B buffer) at the given $(B offset).
			/// .
			/// The stream may reach the end of the file before it can read in $(B count) amount of bytes, if that happens then $(B buffer) won't have all of the data expected.
			/// .
			/// $(B Requires that the stream was opened with access to read from the file)
			/// 
			/// Parameters:
			/// 	buffer = Byte array to store the data read in
			/// 	offset = The offset in $(B buffer) to start storing the read in data.
			/// 	count = How many bytes to read into the file.
			/// 
			/// 
			/// Throws $(B System.NotSupportedException) if the stream doesn't have access to read functions.
			/// Throws $(B System.IO.IOException) if the stream is at the end of the file.
			/// Throws $(B System.ArgumentNullException) if $(B buffer) is null.
			/// Throws $(B System.ArgumentException) if the sum of $(B offset) and $(B count) is greater than the length of $(B buffer)
			/// 
			/// Returns:
			/// 	How many byte were actually read in.
			int Read(inout ubyte[] buffer, int offset, uint count)
			{
				if(!this.CanRead) throw new NotSupportedException("Reading");
				if(this.Position == this.Length) throw new IOException("Cannot read past file");
				if(buffer is null) throw new ArgumentNullException("buffer");
				if((offset + count) > buffer.length) throw new ArgumentException("buffer", "Buffer is too small!");

				return cast(int)fread(cast(void*)buffer[offset..offset + count].ptr, 1, offset + count, this._File.getFP);
			}

			/// Writes $(B count) amount of bytes from a specified $(B offset) in $(B buffer).
			/// .
			/// $(B Requires that the stream was opened with access to write to the file)
			/// 
			/// Parameters:
			/// 	buffer = Byte array that contains the data to write.
			/// 	offset = Offset to where the data to write begins in $(D buffer).
			/// 	count = How many bytes from $(B buffer[offset]) to write into the file.
			/// 
			/// 
			/// Throws $(B System.NotSupportedException) if the stream doesn't have access to write functions.
			/// Throws $(B System.ArgumentNullException) if $(B buffer) is null.
			/// Throws $(B System.ArgumentException) if the sum of $(B offset) and $(B count) is greater than the length of $(B buffer)
			void Write(const(ubyte)[] buffer, int offset, uint count)
			{
				if(!this.CanWrite) throw new NotSupportedException("Writing");
				if(buffer is null) throw new ArgumentNullException("buffer");
				if((offset + count) > buffer.length) throw new ArgumentException("buffer", "Buffer is too small!");

				this._File.rawWrite(buffer[offset..offset + count]);
			}

			/// Change the stream's position in the file by applying $(B offset) to the specified $(B origin) point.
			/// .
			/// $(B Requires that the stream wasn't opened with System.IO.FileMode.Append)
			/// 
			/// Parameters:
			/// 	offset = Offset the apply to $(B origin).
			/// 	origin = Where to apply the offset.
			/// 
			/// 
			/// Throws $(B System.IO.IOException) if the stream was opened with $(B System.IO.FileMode.Append)
			/// 
			/// Returns:
			/// 	The new position of the stream in the file.
			long Seek(long offset, SeekOrigin origin)
			{
				if(!this.CanSeek) throw new IOException("Cannot seek using FileMode.Append");

				this._File.seek(offset, origin);
				return this.Position;
			}

			/// Increases the size of the stream's file so that it is $(B length) bytes large.
			/// .
			/// Cannot make the file shrink(yet).
			/// .
			/// $(B Requires that the stream was opened with access to write to the file)
			/// $(B Requires that the stream wasn't opened with System.IO.FileMode.Append)
			/// 
			/// Parameters:
			/// 	length = The desired length of the file.
			/// 
			/// 
			/// Throws $(B System.ArgumentOutOfRangeException) if $(B length) is smaller than the current size of the file.
			void SetLength(ulong length)
			{
				if(length <= this.Length) throw new ArgumentOutOfRangeException("length", "Length must be bigger than the file's length. (Temporary Exception)");

				// TODO: Allow this method to shrink files
				ulong Difference = length - this.Length;

				ubyte[] Buffer;
				Buffer.length = cast(uint)Difference;

				ulong Temp = this.Position;
				this.Seek(0, SeekOrigin.End);
				this.Write(Buffer, 0, Buffer.length);
				this.Position = Temp;
			}

			/// Returns the next byte that the stream will read in without advancing the stream's position.
			/// .
			/// $(B Requires that the stream was opened with access to write to the file)
			/// 
			/// Returns:
			/// 	The next byte.
			ubyte Peek()
			{
				ubyte ToReturn = cast(ubyte)this.ReadByte();
				this.Position = this.Position - 1;
				return ToReturn;
			}

			@property
			bool CanRead()
			{
				return cast(bool)(this._Access & FileAccess.Read) && (this._Mode != FileMode.Append);
			}
			
			/// When overridden in a derived class, gets a value indicating whether the current stream supports seeking.
			/// 
			/// Returns:
			/// 	$(D true) if the stream supports seeking; otherwise, $(D false).
			@property
			bool CanSeek()
			{
				return (this._Mode != FileMode.Append);
			}
			
			/// When overridden in a derived class, gets a value indicating whether the current stream supports writing.
			/// 
			/// Returns:
			/// 	$(D true) if the stream supports writing; otherwise, $(D false).
			@property
			bool CanWrite()
			{
				return cast(bool)(this._Access & FileAccess.Write);
			}
			
			/// When overridden in a derived class, gets the length in bytes of the stream.
			/// .
			/// $(B Requires that the stream wasn't opened with System.IO.FileMode.Append)
			///
			/// Returns:
			/// 	A $(D long) value representing the length of the stream in bytes.
			@property
			ulong Length()
			{
				ulong Temp = this.Position;

				this.Seek(0, SeekOrigin.End);
				ulong ToReturn = this.Position;
				this.Position = Temp;

				return ToReturn;
			}
			
			/// When overridden in a derived class, gets or sets the position within the current stream.
			/// 
			/// Returns:
			/// 	The current position within the stream.
			@property
			ulong Position()
			{
				return this._File.tell();
			}

			/// When overridden in a derived class, gets or sets the position within the current stream.
			/// .
			/// $(B Requires that the stream wasn't opened with System.IO.FileMode.Append)
			/// 
			/// Returns:
			/// 	The current position within the stream.
			@property
			void Position(long position)
			{
				this.Seek(position, SeekOrigin.Begin);
			}

			/// Gets or sets a value, in miliseconds, that determines how long the stream will attempt to read before timing out.
			/// 
			/// Returns:
			/// 	A value, in miliseconds, that determines how long the stream will attempt to read before timing out.
			@property
			int ReadTimeout()
			{
				return super.ReadTimeout;
			}
			
			/// Ditto
			@property
			void ReadTimeout(int timeout)
			{
				super.ReadTimeout = timeout;
			}
			
			/// Gets or sets a value, in miliseconds, that determines how long the stream will attempt to write before timing out.
			/// 
			/// Returns:
			/// 	A value, in miliseconds, that determines how long the stream will attempt to write before timing out.
			@property
			int WriteTimeout()
			{
				return super.WriteTimeout;
			}
			
			/// Ditto
			@property
			void WriteTimeout(int timeout)
			{
				super.WriteTimeout = timeout;
			}

			/// Gets a value that determines whether the current stream can time out.
			/// 
			/// Returns:
			/// 	A value that determines whether the current stream can time out.
			@property
			bool CanTimeout()
			{
				return false;
			}
		}
	}
}
//void Errors()
unittest
{
	string Name = "unittests/FSTest.bin";
	FileStream FS = new FileStream(Name, FileMode.Create, FileAccess.ReadWrite);

	ubyte[] Data1 = [ 0x0, 0x1, 0xA ];
	ubyte[] Data2 = [ 0xC, 0xB, 0x0 ];

	ubyte[] DataOutput = [ 0x0, 0xC, 0xB, 0x0 ];

	FS.WriteFlush(Data1, 0, Data1.length);
	FS.Position = 1; assert(FS.Position == 1);
	FS.WriteFlush(Data2, 0, Data2.length);

	assert(FS.Length == 4);

	FS.Position = 0;
	ubyte[] Buffer;
	Buffer.length = DataOutput.length;
	assert(FS.Read(Buffer, 0, Buffer.length) == Buffer.length);

	assert(DataOutput == Buffer);
	FS.Dispose(true);
}