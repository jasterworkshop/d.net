﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.Path;

private
{
	import std.regex, std.path, std.traits;

	import System.String;
}

public
{
	import System.ArgumentNullException;
}

/// TODO: Work on this some more, this was just made so far for ResolvePath
static class Path
{
	private static
	{
	}

	public static
	{
		/// Gets the full, resolved path to $(B filePath).
		/// .
		/// This function also handles "." and ".." in paths.
		/// 
		/// Parameters:
		/// 	filePath = The initial path
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B filePath) is null.
		/// 
		/// Returns:
		/// 	The full file path to $(B filePath)
		string ResolvePath(string filePath)
		{
			if(filePath is null) throw new ArgumentNullException("filePath");

			// I couldn't get buildNormalizedPath to work correctly, so I'm doing it myself.

			// Get the Unresolve path
			auto Unresolved = absolutePath(filePath);
			string[] Tree;
			string Buffer = "";

			// Handles buffer parsing thing
			void DoBufferThing()
			{
				// As long as we have something in the buffer, at least
				if(Buffer != "")
				{
					// Handle "." and ".."
					if(Buffer == "." || Buffer == "..")
					{
						if(Tree.length != 0) Tree.length -= 1;
						Buffer = "";
						return;
					}
					
					// Add the Thing to the tree
					Tree ~= Buffer;
					Buffer = "";
				}
			}

			for(uint i = 0; i < Unresolved.length; i++)
			{
				auto Current = Unresolved[i];

				// If we hit either one of the things below, then add thingy to the Tree
				if(Current == '/' || Current == '\\')
				{
					DoBufferThing();
				}
				else
				{
					Buffer ~= Current;
				}
			}
			DoBufferThing(); // Because the buffer will have the final path in it still

			string ToReturn;

			// Then rebuild the path
			foreach(path; Tree)
			{
				// Annoying Convention
				version(Windows)
				{
					ToReturn ~= (path ~ "\\");
				}
				else
				{
					ToReturn ~= (path ~ "/");
				}
			}
			ToReturn.length -= 1;// Remove the final "/" or "\"

			return ToReturn;
		}

		/// Changes the extension of $(B path) to $(B extension).
		/// 
		/// Parameters:
		/// 	path = The path to change the extension of.
		/// 	extension = The extension to give the path.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if either parameter is null
		/// 
		/// Returns:
		/// 	$(B path) with it's extension as $(B extension).
		S ChangeExtension(S)(S path, S extension)
		if(isSomeString!S)
		{
			if(path is null) throw new ArgumentNullException("path");
			if(extension is null) throw new ArgumentNullException("extension");

			return setExtension(path, extension);
		}

		/// Combines two paths together.
		/// 
		/// Parameters:
		/// 	path1 = The path to be on the left-most side of the output.
		/// 	path2 = The path to be on the right-most side of the output.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if either parameter is null.
		/// 
		/// Returns:
		/// 	$(B path1) combined with $(B path2).
		S Combine(S)(S path1, S path2)
		if(isSomeString!S)
		{
			if(path1 is null || path2 is null) throw new ArgumentNullException((path1 is null) ? "path1" : "path2");

			// Remove the starting "/" or "\" or "\\" from path2, because I want path1 to have that thing. Also Duplicated path1 because we're gonna be making edits to it, and don't want to edit the user's version.
			auto Path1 = path1.dup;
			auto Path2 = path2.TrimStart("\\/");

			// Put a "/" or "\" at the end of path1 if it doesn't already have one
			if(Path1[$ - 1] != '/' && Path1[$ - 1] != '\\')
			{
				version(Windows)
				{
					Path1 ~= "\\";
				}
				else
				{
					Path1 ~= "/";
				}
			}

			// Then return the "sum"
			return (Path1 ~ Path2);
		}
	}
}
unittest
{
	import std.stdio;

	string FilePath = "Tahn.bin";
	assert(Path.ChangeExtension(FilePath, "txt") == "Tahn.txt");
}