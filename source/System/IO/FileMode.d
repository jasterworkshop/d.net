﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.FileMode;

enum FileMode : ubyte
{
	/// Specifies that the operating system should create a new file. If the file already exists, a $(B System.IO.IOException) exception is thrown.
	CreateNew = 1,
	/// Specifies that the operating system should create a new file. If the file already exists, it will be overwritten. $(B System.IO.FileMode.Create) is equivalent to requesting that if the file does not exist, use $(B System.IO.FileMode.CreateNew); otherwise, use $(B System.IO.FileMode.Truncate).
	Create,
	/// Specifies that the operating system should open an existing file. The ability to open the file is dependent on the value specified by the $(B System.IO.FileAccess) enumeration. A $(B System.IO.FileNotFoundException) exception is thrown if the file does not exist.
	Open,
	/// Specifies that the operating system should open a file if it exists; otherwise, a new file should be created.
	OpenOrCreate,
	/// Opens the file if it exists and seeks to the end of the file, or creates a new file. $(B System.IO.FileMode.Append) can be used only in conjunction with $(B System.IO.FileAccess.Write). Trying to seek to a position before the end of the file throws a $(B System.IO.IOException) exception, and any attempt to read fails and throws a $(B System.NotSupportedException) exception.
	Append
}

