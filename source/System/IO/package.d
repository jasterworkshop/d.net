﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
/// Publically imports all of the modules under System.IO
module System.IO;

public
{
	import System.IO.IOException, System.IO.Stream, 
		System.IO.FileMode, System.IO.BinaryWriter,
		System.IO.FileNotFoundException, System.IO.FileAccess,
		System.IO.FileStream, System.IO.BinaryReader,
		System.IO.StringStream, System.IO.MemoryStream,
		System.IO.EndOfStreamException, System.IO.FileSystemInfo,
		System.IO.FileInfo, System.IO.Path;
}