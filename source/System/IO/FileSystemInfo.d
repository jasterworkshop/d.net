﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.FileSystemInfo;

private
{
	import std.path;
}

abstract class FileSystemInfo
{
	private
	{

	}

	public
	{

		/// The full path of the directory or file.
		@property
		string FullName()
		{
			// When DirectoryInfo is implemented, uncomment all of this
//			if(is(this == DirectoryInfo))
//			{
			// Get Full path to directory
//			}
//			else
//			{
				return this.FullPath;
//			}
		}

		/// Gets the exension part of the file(Excluding the dot).
		@property 
		string Extension()
		{
			return extension(this.FullPath);
		}

		/// The name of the file or directory.
		@property
		abstract string Name();

		/// Returns a value of whether the file or directory exists.
		@property
		abstract bool Exists();

		/// Deletes the File or Directory
		abstract void Delete();
	}

	protected
	{
		/// The fully qualified path of the directory or file.
		string	FullPath;

		/// The path specified by the user.
		string	OriginalPath;
	}
}

