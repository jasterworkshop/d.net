﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.File;

private
{
	import std.file : read, readText, write;
	import std.string : splitLines;
	import std.traits : isSomeString;

	import System.IO.Path, System.IO.FileInfo;
}

public
{
	import System.IO.FileStream, System.ArgumentNullException;
}

static class File
{
	private static
	{
	}

	public static
	{
		/// Creates or overwrites a file.
		/// 
		/// Parameters:
		/// 	filePath = Where to create/overwrite the file.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B filePath) is null.
		/// 
		/// Returns:
		/// 	A $(B System.IO.FileStream) object that has read/write access to the newly created file.
		FileStream Create(string filePath)
		{
			if(filePath is null) throw new ArgumentNullException("filePath");

			return new FileStream(Path.ResolvePath(filePath), FileMode.Create, FileAccess.ReadWrite);
		}

		/// Copies the a file to another location.
		/// 
		/// Parameters:
		/// 	sourceFile = The file to copy
		/// 	destinationFile = The location to copy $(B sourceFile) to
		/// 	overwrite = True if $(B Copy) should overwrite a file if one exists at $(B destinationFile). False otherwise.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if either $(B sourceFile) or $(B destinationFile) are null.
		/// Throws $(B System.IO.FileNotFoundException) if $(B sourceFile) doesn't exist.
		/// Throws $(B System.IO.FileException) if there is a file at $(B destinationFile) and $(B overwrite) is false.
		void Copy(string sourceFile, string destinationFile, bool overwrite = false)
		{
			if(sourceFile is null) throw new ArgumentNullException("sourceFile");
			if(destinationFile is null) throw new ArgumentNullException("destinationFile");

			// The reason I use FileInfo in so many of these methods is simply because they already have the code for these things.
			auto FI = new FileInfo(sourceFile);
			FI.CopyTo(destinationFile, overwrite);
		}

		/// Deletes a file.
		/// 
		/// Parameters:
		/// 	file = The file to delete
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B file) is null.
		void Delete(string file)
		{
			if(file is null) throw new ArgumentNullException("file");

			new FileInfo(file).Delete();
		}

		/// Determines whether a file exists.
		/// 
		/// Parameters:
		/// 	file = The file to check that exists.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B file) is null.
		/// 
		/// Returns:
		/// 	True if a file exists at the path $(B file). False otherwise.
		bool Exists(string file)
		{
			return (new FileInfo(file).Exists);
		}

		/// Moves a specified file to a new location, overwriting any existing file.
		/// 
		/// Parameters:
		/// 	sourceFile = The file to move.
		/// 	destinationFile = The location to move the file to. If a file already exists here, it is overwritten.
		/// 
		/// 
		/// Throws $(B System.IO.FileNotFoundException) if $(B sourceFile) doesn't exist.
		void Move(string sourceFile, string destinationFile)
		{
			new FileInfo(sourceFile).MoveTo(destinationFile);
		}

		/// Opens a file using the given $(B System.IO.FileStream) arguments.
		/// 
		/// Parameters:
		/// 	file = The file to open.
		/// 	mode = What mode to open the file in
		/// 	access = What operations the $(B System.IO.FileStream) has access to.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B file) is null.
		/// 
		/// Returns:
		/// 	A FileStream that was opened from the file at $(B file).
		FileStream Open(string file, FileMode mode = FileMode.OpenOrCreate, FileAccess access = FileAccess.ReadWrite)
		{
			if(file is null) throw new ArgumentNullException("file");

			return (new FileInfo(file).Open(mode, access));
		}

		/// Opens an existing file for reading.
		/// 
		/// Parameters:
		/// 	file = The file to open
		/// 
		/// 
		/// Throws $(B System.IO.FileNotFoundException) if $(B file) doesn't point to an existing file.
		/// Throws $(B System.ArgumentNullException) if $(B file) is null.
		/// 
		/// Returns:
		/// 	A $(B System.IO.FileStream) object that has read access to the file at $(B file).
		FileStream OpenRead(string file)
		{
			return File.Open(file, FileMode.Open, FileAccess.Read);
		}

		/// Opens/Creats a file for writing.
		/// 
		/// Parameters:
		/// 	file = The file to open
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B file) is null.
		/// 
		/// Returns:
		/// 	A $(B System.IO.FileStream) object that has write access to the file at $(B file).
		FileStream OpenWrite(string file)
		{
			return File.Open(file, FileMode.OpenOrCreate, FileAccess.Write);
		}

		/// Reads the contents of a specified file and returns it as an array of bytes.
		/// 
		/// Parameters:
		/// 	file = The file to read the contents of.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B file) is null.
		/// Throws $(B System.IO.FileNotFoundExcetpion) if $(B file) doesn't point to an existing file.
		/// 
		/// Returns:
		/// 	The contents of the file at $(B file).
		ubyte[] ReadAllBytes(string file)
		{
			string F = Path.ResolvePath(file); // ResolvePath does the null check

			if(!File.Exists(F)) throw new FileNotFoundException(F);

			return cast(ubyte[])read(F);
		}

		/// Reads the lines of a specified file and returns it as an array of T. Each member in the array represents 1 line of the text file.
		/// 
		/// Parameters:
		/// 	file = The file to read the contents of.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B file) is null.
		/// Throws $(B System.IO.FileNotFoundExcetpion) if $(B file) doesn't point to an existing file.
		/// 
		/// Returns:
		/// 	The lines of the file at $(B file).
		T[] ReadAllLines(T)(string file)
		if(isSomeString!T)
		{
			string F = Path.ResolvePath(file);

			if(!File.Exists(F)) throw new FileNotFoundException(F);

			return readText!T(F).splitLines!T();
		}

		/// Reads the text of a specified file and returns it as a T.
		/// 
		/// Parameters:
		/// 	file = The file to read the contents of.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B file) is null.
		/// Throws $(B System.IO.FileNotFoundExcetpion) if $(B file) doesn't point to an existing file.
		/// 
		/// Returns:
		/// 	The text of the file at $(B file).
		T ReadAllText(T)(string file)
		if(isSomeString!T)
		{
			string F = Path.ResolvePath(file);

			if(!File.Exists(F)) throw new FileNotFoundException(F);

			return readText!T(F);
		}

		/// Writes the given Data to a specified file.
		/// 
		/// Parameters:
		/// 	file = The file to write to.
		/// 	data = The data to write.
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B file) is null.
		void WriteAllBytes(string file, ubyte[] data)
		{
			write(Path.ResolvePath(file), data);
		}

		/// Writes the given lines of text to a specified file.
		/// 
		/// Parameters:
		/// 	file = The file to write to.
		/// 	data = The lines of text to write.
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B file) is null.
		void WriteAllLines(T)(string file, T[] data)
		if(isSomeString!T)
		{
			T Data;

			foreach(str; data) Data ~= (str ~ "\n");

			write(Path.ResolvePath(file), Data);
		}

		/// Writes the given text to a specified file.
		/// 
		/// Parameters:
		/// 	file = The file to write to.
		/// 	data = The text to write.
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B file) is null.
		void WriteAllText(T)(string file, T data)
		if(isSomeString!T)
		{
			write(Path.ResolvePath(file), data);
		}
	}
}
unittest
{
	string FileName = "unittests/Dabiel.txt";

	ubyte[] BData = [0xFF, 0x20, 0x60];
	string SData = "Soupeh";
	wstring WSData = "Word Soupeh";
	dstring DSData = "Double Word Soupeh";

	File.Create(FileName).Close();
	assert(File.Exists(FileName));

	File.Delete(FileName);
	assert(!File.Exists(FileName));

	File.WriteAllBytes(FileName, BData);
	assert(File.ReadAllBytes(FileName) == BData);

	File.WriteAllText(FileName, SData);
	assert(File.ReadAllText!string(FileName) == SData);
	File.WriteAllText(FileName, WSData);
	assert(File.ReadAllText!wstring(FileName) == WSData);
	File.WriteAllText(FileName, DSData);
	assert(File.ReadAllText!dstring(FileName) == DSData);

	File.WriteAllLines(FileName, [WSData, WSData]);
	assert(File.ReadAllLines!wstring(FileName) == [WSData, WSData]);

	File.Delete(FileName);
	File.Create(FileName).Close;

	auto FS = File.OpenWrite(FileName);
	FS.Write(BData, 0, BData.length);
	FS.Close();

	ubyte[] Buffer; Buffer.length = BData.length;
	FS = File.OpenRead(FileName);
	assert(FS.Read(Buffer, 0, Buffer.length) == Buffer.length);
	assert(Buffer == BData);
	FS.Close();
}