﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
/// This class isn't part of the .Net framework, it's something I made a while ago that I feel is useful.
module System.IO.StringStream;

private
{
	import std.conv;
}

public
{
	import System.IO.EndOfStreamException, System.ArgumentNullException;
}

// TODO: Make the write functions for this class
/// Provides an interface to write and read strings using other Basic data types
/// 
/// Methods may return "Char" or "String", "Char" being a single T character, and "String" being an array of T characters
class StringStream(T)
if(is(T == char) || is(T == wchar) || is(T == dchar))
{
	private
	{
		/// The stream's buffer
		String _Data;
		
		/// Used to check if a character is a digit
		String _Digits = "0123456789.";
		
		/// Position the stream is at in it's buffer
		uint _Position = 0;
		uint _Line = 1;
		uint _Column = 1;
		
		alias immutable(T) 	Char;
		alias Char[] 		String;
		
		/// Keeps reading in characters as long as they can be converted into an integer.
		T2 ReadNumber(T2)()
		{
			return to!T2(this.ReadNumericalString(false));
		}
		
		/// Keeps reading in characters as long as they can be converted into a decimal.
		T2 ReadDecimal(T2)()
		{
			return to!T2(this.ReadNumericalString(true));
		}
		
		/// Grrr.... Tiny names, but I'd rather have them instead of "_string" "_char" #std.algrorithm.amongYUNoWork
		bool Contains(Char c, String s)
		{
			foreach(ch; s)
			{
				if(ch == c)
				{
					return true;
				}
			}
			
			return false;
		}
	}
	
	public
	{		
		/// If true, $(B ReadChar) and any other function calling it, will parse strings such as "\\r \\n" into [ '\r', ' ', '\n' ]
		bool ParseEscapeCharacters = false;
		
		/// Constructs a StringStream with the given string as a buffer
		/// 
		/// Parameters: 
		/// 	input = The string to use as a buffer
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B input) is null.
		this(String input)
		{
			if(input is null) throw new ArgumentNullException("input");

			this._Data = input;
		}
		
		/// Returns the next character without advancing the position
		/// 
		/// Returns:
		/// 	The next character in the buffer.
		Char Peek()
		{
			/// If we're at the end of the buffer, return null(We don't need an exception if we're only peeking)
			if(this.EoF)
			{
				return '\0';
			}
			
			/// Otherwise, return the character
			return this._Data[this._Position];
		}
		
		/// Skips past any space, new line, and tab until the stream hits a not mentioned character
		void SkipBlanks()
		{
			/// While we're not EoF, and we're over a whitespace character, advance the stream
			while(!this.EoF && (this.Peek() == ' ' || this.Peek() == '\n' || this.Peek() == '\t' || this.Peek() == cast(char)0x0D || this.Peek() == '\r'))
			{
				this.ReadChar();
			}
		}
		
		/// Writes $(B toWrite) at the current position in the file
		/// 
		/// Parameters:
		/// 	toWrite = The character to write
		void WriteChar(Char toWrite)
		{
			/// Turn the buffer into a mutable form
			T[] Data = cast(T[])this._Data;
			
			/// Set the character
			Data[this._Position++] = toWrite;
			
			/// Then set the data back
			this._Data = cast(String)Data;
		}
		
		/// Returns the next character in the string, advancing the position of the stream.
		/// 
		/// 
		/// Throws $(B System.IO.EndOfStreamException) if an attempt to read past the Stream's buffer is made.
		/// 
		/// Returns:
		/// 	The next character in the buffer.
		Char ReadChar()
		{
			//			writeln("Here? ", this.Peek(), " ", this.ParseEscapeCharacters, " \\");
			//			readln();
			
			/// If we're at the end of the buffer, return null
			if(this.EoF)
			{
				throw new EndOfStreamException("Attempting to read past the Stream's buffer.");
			}
			
			/// Escape character handler
			if(this.ParseEscapeCharacters == true && this.Peek() == '\\')
			{
				/// Skip past the '\'
				this._Position++;
				
				/// Get the escape character
				Char Escape = this.ReadChar();
				
				switch(Escape)
				{
					case 'n':
						return '\n';
						
					case 'r': return '\r';
					case '\\': return '\\';
					case 't': return '\t';
					case '"': return '"';
						
					default:
						break;
				}
			}
			
			/// If we hit a new line, reset the Column counter and add one to the Line counter
			if(this.Peek() == '\n')
			{
				this._Line += 1;
				this._Column = 1;
			}
			else
			{
				/// Otherwise, just add one to the Column counter
				this._Column += 1;
			}
			
			/// Otherwise, return the character
			return this._Data[this._Position++];
		}
		
		/// Attempts to read $(B length) amount of characters, and returns them
		/// The String's length may be less than the requested length, as EoF could be hit
		/// 
		/// Parameters:
		/// 	length = How many characters the stream should try to read in and return
		/// 
		/// Returns:
		/// 	$(B length) amount of characters... If $(B EoF) wasn't hit.
		String ReadChars(uint length)
		{
			/// String to return
			String ToReturn;
			
			/// Read until we hit EoF, or we've read in enough characters
			while(!this.EoF && ToReturn.length < length)
			{
				ToReturn ~= this.ReadChar();
			}
			
			return ToReturn;
		}
		
		/// Keep reading characters until one of the characters in $(B delimiters) is hit, or until EoF is hit, then return those characters
		/// 
		/// Parameters:
		/// 	delimiters = The characters to look out for.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B delimiters) is null.
		/// 
		/// Returns:
		/// 	A string that contains the characters read in before hitting a character in $(B delimiter), or EoF
		String ReadTo(Char[] delimiters)
		{
			if(delimiters is null) throw new ArgumentNullException("delimiters");

			/// String to return
			String ToReturn;
			
			/// See the description of the method for conditions
			while(!this.EoF && !this.Contains(this.Peek(), delimiters))
			{
				ToReturn ~= this.ReadChar();
			}
			
			return ToReturn;
		}
		
		/// Reads characters that take the form of a number
		/// Decimal is used to determine if to also read in decimal characters
		/// 
		/// Parameters:
		/// 	decimal = If true, the character '.' will be counted as a character to read in
		/// 
		/// Returns:
		/// 	A string containing only numerical characters.
		String ReadNumericalString(bool decimal)
		{
			String ToReturn;
			
			// #AmazingSliceOfDeathBecauseI'mTooLazyToMakeAnIf
			// It basically makes sure that the "." at the end of "_Digits" is passed to "Contains" if "decimal" is true 
			while(!this.EoF && Contains(this.Peek(), this._Digits[0..$ - (cast(uint)(!decimal))]))
			{
				ToReturn ~= this.ReadChar();
			}
			
			return ToReturn;
		}

		/// Reads a Numerical string and converts it into an Unsigned byte
		alias this.ReadNumber!(ubyte)  	ReadByte;

		/// Reads a Numerical string and converts it into a 16-bit Unsigned Integer
		alias this.ReadNumber!(ushort)	ReadShort;

		/// Reads a Numerical string and converts it into a 32-bit Unsigned Integer
		alias this.ReadNumber!(uint)   	ReadInt;

		/// Reads a Numerical string and converts it into a 64-bit Unsigned Integer
		alias this.ReadNumber!(ulong)  	ReadLong;

		/// Reads a Numerical string and converts it into a Single-precision floating point number
		alias this.ReadDecimal!(float)  ReadFloat;

		/// Reads a Numerical string and converts it into a Double-precision floating point number
		alias this.ReadDecimal!(double) ReadDouble;
		
		/// Gets the Stream's buffer
		@property
		nothrow String Buffer()
		{
			return this._Data;
		}
		
		/// Sets the stream's position in it's buffer
		@property
		nothrow void Position(uint position) 
		{
			this._Position = position;
		}
		
		/// Gets the Stream's position that it's at for it's buffer
		@property
		nothrow uint Position() 
		{
			return this._Position;
		}
		
		/// Gets the Stream's line
		@property
		nothrow uint Line() 
		{
			return this._Line;
		}
		
		/// Gets the Stream's column(Which character of the line it's at)
		@property
		nothrow uint Column()
		{
			return this._Column;
		}
		
		/// Gets if the stream is at the end of it's buffer
		@property
		nothrow bool EoF() 
		{
			return this.Position >= this._Data.length;
		}
	}
}
unittest
{
	// Make the Stream
	auto Stream = new StringStream!dchar("P ToColon:\n12\n14.2\n"d);
	
	assert(Stream.Peek() == 'P');
	assert(Stream.ReadChar() == 'P');
	
	Stream.SkipBlanks();
	assert(Stream.Position == 2, to!string(Stream.Position));
	
	assert(Stream.ReadTo(":") == "ToColon"d); assert(Stream.ReadChar() == ':');
	Stream.SkipBlanks();
	
	assert(Stream.ReadInt() == 12);
	Stream.SkipBlanks();
	
	// For whatever reason, I can't use "Stream.ReadFloat" in the assert, otherwise it fails p_p
	float Ples = Stream.ReadFloat();
	
	assert(Ples == 14.2f);
	assert(Stream.Column == 5);
	Stream.SkipBlanks();
	
	assert(Stream.Line == 4);
	assert(Stream.EoF == true);
}