﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.FileInfo;

private
{
	import std.path, std.file, std.regex;

	import System.String, System.IO.Path;
}

public
{
	import System.IO.FileSystemInfo, System.IO.FileNotFoundException,
		System.ArgumentNullException, System.IO.FileStream;
}

final class FileInfo : FileSystemInfo
{
	private
	{
		// TODO: DirectoryInfo	_Directory
		string	_DirectoryName;
	}

	public
	{
		/// Constructs a new $(B FileInfo) that provides info for $(B filePath)
		this(string filePath)
		{
			if(filePath is null) throw new ArgumentNullException("filePath");
			this.OriginalPath = filePath;
			this.FullPath = Path.ResolvePath(filePath);

			// This one is a bit annoying as well <3. I have to split up the dirName's output and then return the last member, as it returns the entire file path, just without the file in it.
			this._DirectoryName = dirName(this.FullPath).Replace("\\", "/").Split("/", StringSplitOptions.RemoveEmptyEntries)[$ - 1];
		}

		/// Copies the file the current $(B FileInfo) object points to, to the file at $(B destinationFilePath). If a file exists at the destination, then it can be spcified whether or not to overwrite it or not.
		/// 
		/// Parameters:
		/// 	destinationFilePath = The file to copy the current $(B FileInfo)'s file to.
		/// 	overwrite = True if a file should be overwritten if one exists at the destination, false otherwise.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B destinationFilePath) is null.
		/// Throws $(B System.IO.FileException) if a file exists at the destination path and $(B overwrite) is false.
		/// 
		/// Returns:
		/// 	A new $(B FileInfo) object that is wrapped around the new file at $(B destinationFilePath)
		FileInfo CopyTo(string destinationFilePath, bool overwrite = false)
		{
			if(destinationFilePath is null) throw new ArgumentNullException("destinationFilePath");

			string Path = Path.ResolvePath(destinationFilePath);
			if(!overwrite && exists(Path)) throw new FileException("Overwriting files was disallowed");

			copy(this.FullPath, Path);
			return new FileInfo(Path);
		}

		/// Creates a file. If one already exists, it will be overwritten.
		/// 
		/// Returns:
		/// 	A $(B System.IO.FileStream) for the new file created
		FileStream Create()
		{
			return new FileStream(this.FullName, FileMode.Create);
		}

		/// Opens the file that the current $(B FileInfo) object points to, using the given $(B System.IO.FileStream) arguments.
		/// 
		/// Parameters:
		/// 	mode = What mode to open the file in
		/// 	access = What operations the $(B System.IO.FileStream) has access to.
		/// 
		/// Returns:
		/// 	A FileStream that was opened from the $(B FileInfo)'s file.
		FileStream Open(FileMode mode = FileMode.OpenOrCreate, FileAccess access = FileAccess.ReadWrite)
		{
			return new FileStream(this.FullName, mode, access);
		}

		/// Moves the file that the current $(B FileInfo) object points to, to the file at $(B filePath). If a file exists at $(B filePath) then it is overwritten.
		/// 
		/// Parameters:
		/// 	filePath = The destination to move the current $(B FileInfo)'s file to.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B filePath) is null.
		/// Throws $(B System.IO.FileNotFoundException) if the current $(B FileInfo) object's file doesn't exist.
		void MoveTo(string filePath)
		{
			if(filePath is null) throw new ArgumentNullException("filePath");
			if(!this.Exists) throw new FileNotFoundException(this.FullPath);

			this.CopyTo(Path.ResolvePath(filePath), true);
			this.Delete();
		}

		/// Deletes the file that the current $(B FileInfo) object points to. If the file doesn't exist, then nothing happens.
		override void Delete()
		{
			if(this.Exists)
			{
				remove(this.FullPath);
			}
		}

		/// The Name + Extension of the file
		@property
		override string Name()
		{
			return baseName(this.FullPath);
		}

		/// Returns whether the file exists or not
		@property
		override bool Exists() 
		{
			return exists(this.FullPath);
		}

		/// Returns the name of the directory the file is in
		@property
		string DirectoryName()
		{
			return this._DirectoryName;
		}
	}
}
unittest
{
	auto File1 = new FileInfo("unittests/Dan.txt");
	File1.Delete(); // Make sure it's not there <3

	assert(!File1.Exists);
	File1.Create().Close();
	assert(File1.Exists);
	assert(File1.DirectoryName == "unittests", "Directory: " ~ File1.DirectoryName);
	assert(File1.Name == "Dan.txt", "Name: " ~ File1.Name);

	auto File2 = File1.CopyTo("unittests/Dan2.txt", true);
	assert(File2.Exists);

	File1.Delete();
	assert(!File1.Exists);

	File2.MoveTo(File1.FullName);
	assert(File1.Exists);
	assert(!File2.Exists);
}