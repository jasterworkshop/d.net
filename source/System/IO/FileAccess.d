﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.FileAccess;

public enum FileAccess : ubyte
{
	/// Read access to the file. Data can be read from the file. Combine with Write for read/write access.
	Read = 1,
	/// Write access to the file. Data can be written to the file. Combine with Read for read/write access.
	Write = 2,
	/// Read and write access to the file. Data can be written to and read from the file.
	ReadWrite = 3
}