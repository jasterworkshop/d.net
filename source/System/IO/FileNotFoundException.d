﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.FileNotFoundException;

private 
{
	import std.string : format;
	import System.IO.IOException;
}

class FileNotFoundException : IOException
{
	this(string file)
	{
		super(format("The file '%s' was not found!", file));
	}
}

