﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.IO.MemoryStream;

public
{
	import System.IO.Stream, System.NotSupportedException,
		System.ArgumentException, System.ArgumentNullException,
		System.InvalidOperationException, System.IO.EndOfStreamException;
}

class MemoryStream : Stream
{
	private
	{
		ubyte[]		_Buffer;
		uint		_Index;
	}

	public
	{
		this()
		{
		}
		
		this(ubyte[] buffer)
		{
			this._Buffer = buffer;
		}

		/// Returns the buffer of the stream
		@property
		ubyte[] Buffer()
		{
			return this._Buffer;
		}
	}

	public override
	{
		/// Closes the current stream and releases any resources (such as sockets and file handles) associated with the current stream.
		void Close()
		{
			this.Dispose(true);
			this.destroy();
		}
		
		/// Releases the unmanaged resources used by the $(B Stream.IO.Stream) and optionally releases the managed resources.
		/// 
		/// Parameters:
		/// 	disposing = $(D true) to release both managed and unmanaged resources; $(D false) to release only unmanaged resources.</param>
		void Dispose(bool disposing)
		{
			import core.memory;
			if(disposing && this._Buffer.length != 0)
			{
				GC.free(this._Buffer.ptr);
			}
		}
		
		/// When overridden in a derived class, clears all buffers for this stream and causes any buffered data to be written to the underlying device.
		void Flush() { throw new NotSupportedException(); }
		
		/// When overridden in a derived class, reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
		/// 
		/// Parameters:
		/// 	buffer = An array of bytes. When this method returns, the buffer contains the specified byte array with the values between $(B offset) and ($(B offset) + $(B count) - 1) replaced by the bytes read from the current source.
		/// 	offset = The zero-based byte offset in $(B buffer) at which to begin storing the data read from the current stream.
		/// 	count = The maximum number of bytes to be read from the current stream.
		/// 
		/// Throws $(B System.IO.EndOfStreamException) if an attempt to read past the stream's buffer is made.
		/// Throws $(B System.ArgumentNullException) if $(B buffer) is $(B null)
		/// 
		/// Returns:
		/// 	The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.
		int Read(inout ubyte[] buffer, int offset, uint count)
		{
			this._Index += count;
			if(this._Index > this._Buffer.length) throw new EndOfStreamException("read");
			if(buffer is null) throw new ArgumentNullException("buffer");

			(cast(ubyte[])buffer)[offset..offset + count] = this._Buffer[this._Index - count..this._Index];
			return count;
		}

		/// Returns:
		/// 	The next byte in the stream's buffer without advancing the stream's position.
		ubyte Peek(){return this._Buffer[this._Index];}
		
		/// When overridden in a derived class, sets the position within the current stream.
		/// 
		/// Parameters:
		/// 	offset = A byte offset relative to the $(B origin) parameter.
		/// 	origin = A value of type $(B System.IO.SeekOrigin) indicating the reference point used to obtain the new position.
		/// 
		/// Throws $(B System.IO.EndOfStreamException) if an attempt to seek past the Stream's buffer is made.
		/// 
		/// Returns:
		/// 	The new position within the current stream.
		long Seek(long offset, SeekOrigin origin)
		{
			switch(origin)
			{
				case SeekOrigin.Begin:
					this._Index = cast(uint)offset;
					break;

				case SeekOrigin.Current:
					this._Index += offset;
					break;

				case SeekOrigin.End:
					this._Index = cast(uint)((this._Buffer.length - 1) + offset);
					break;

				default:
					break;
			}

			if(this._Index >= this._Buffer.length) throw new EndOfStreamException("seek");
			return this._Index;
		}
		
		/// When overridden in a derived class, sets the length of the current stream.
		/// 
		/// Parameters:
		/// 	length = The desired length of the current stream in bytes.
		void SetLength(ulong length)
		{
			this._Buffer.length = cast(uint)length;
		}
		
		/// When overridden in a derived class, writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
		/// 	buffer = An array of bytes. This method copies $(B count) bytes from $(B buffer) to the current stream.
		/// 	offset = The zero-based byte offset in $(B buffer) at which to begin copying bytes to the current stream.
		/// 	count = The number of bytes to be written to the current stream.
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B buffer) is $(B null)
		void Write(const(ubyte)[] buffer, int offset, uint count)
		{
			this._Index += count;
			if(this._Index >= this._Buffer.length) this.SetLength(this._Index);
			if(buffer is null) throw new ArgumentNullException("buffer");

			this._Buffer[this._Index - count..this._Index] = buffer[offset..offset + count];
		}

		/// When overridden in a derived class, gets a value indicating whether the current stream supports reading.
		/// 
		/// Returns:
		/// 	$(D true) if the stream supports reading; otherwise, $(D false).
		@property
		bool CanRead() {return true;}
		
		/// When overridden in a derived class, gets a value indicating whether the current stream supports seeking.
		/// 
		/// Returns:
		/// 	$(D true) if the stream supports seeking; otherwise, $(D false).
		@property
		bool CanSeek() {return true;}
		
		/// Gets a value that determines whether the current stream can time out.
		/// 
		/// Returns:
		/// 	A value that determines whether the current stream can time out.
		@property
		bool CanTimeout()
		{
			return false;
		}
		
		/// When overridden in a derived class, gets a value indicating whether the current stream supports writing.
		/// 
		/// Returns:
		/// 	$(D true) if the stream supports writing; otherwise, $(D false).
		@property
		bool CanWrite(){return true;}
		
		/// When overridden in a derived class, gets the length in bytes of the stream.
		///
		/// Returns:
		/// 	A $(D long) value representing the length of the stream in bytes.
		@property
		ulong Length(){return cast(ulong)this._Buffer.length;}
		
		/// When overridden in a derived class, gets or sets the position within the current stream.
		/// 
		/// Returns:
		/// 	The current position within the stream.
		@property
		ulong Position(){return cast(ulong)this._Index;}
		
		/// Deprecated for now. For whatever it doesn't do a single thing, so just use $(B System.IO.MemoryStream.Seek) for now
		@property deprecated("See the comments")
		void Position(long position){this.Seek(Position, SeekOrigin.Begin);}
		
		/// Gets or sets a value, in miliseconds, that determines how long the stream will attempt to read before timing out.
		/// 
		/// Returns:
		/// 	A value, in miliseconds, that determines how long the stream will attempt to read before timing out.
		@property
		int ReadTimeout()
		{
			throw new InvalidOperationException("Timeouts not supported!");
		}
		
		/// Ditto
		@property
		void ReadTimeout(int timeout)
		{
			throw new InvalidOperationException("Timeouts not supported!");
		}
		
		/// Gets or sets a value, in miliseconds, that determines how long the stream will attempt to write before timing out.
		/// 
		/// Returns:
		/// 	A value, in miliseconds, that determines how long the stream will attempt to write before timing out.
		@property
		int WriteTimeout()
		{
			throw new InvalidOperationException("Timeouts not supported!");
		}
		
		/// Ditto
		@property
		void WriteTimeout(int timeout)
		{
			throw new InvalidOperationException("Timeouts not supported!");
		}
	}
}

unittest
{
	auto Mem = new MemoryStream();
	ubyte[] ToWrite = [0xAA, 0xBB, 0xCC];
	ubyte[] Buffer;
	Buffer.length = ToWrite.length;

	Mem.Write(ToWrite, 0, ToWrite.length);
	Mem.Seek(0, SeekOrigin.Begin);
	assert(Mem.Read(Buffer, 0, Buffer.length) == ToWrite.length);
	assert(Buffer == ToWrite);
}