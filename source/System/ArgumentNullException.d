﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.ArgumentNullException;

private import System.ArgumentException;

/// The exception that is thrown when a null reference is passed to a method that does not accept it as a valid argument.
class ArgumentNullException : ArgumentException
{
	this(string paramName)
	{
		super(paramName, "Argument is null");
	}

	this(string paramName, string message)
	{
		super(paramName, message);
	}
}