﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.NotSupportedException;

private import System.SystemException;

class NotSupportedException : SystemException
{
	this()
	{
		this("Operation");
	}

	this(string message)
	{
		super(message ~ " not supported!");
	}
}

