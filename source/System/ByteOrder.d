﻿/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */
module System.ByteOrder;

enum ByteOrder : ubyte
{
	Little = 0,
	Big = 1
}

version(BigEndian)
{
	const static ByteOrder NativeByteOrder = ByteOrder.Big;
}
else
{
	const static ByteOrder NativeByteOrder = ByteOrder.Little;
}