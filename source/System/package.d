﻿
/**
 * 
 * //          Copyright SealabJaster 2015
 * // Distributed under the Boost Software License, Version 1.0.
 * //    (See accompanying file LICENSE_1_0.txt or copy at
 * //          http://www.boost.org/LICENSE_1_0.txt)
 * 
 *//// Publically imports all of the modules under System
module System;

public 
{
	import System.String, System.ArgumentNullException,
		System.Char, System.ArgumentOutOfRangeException,
		System.ArgumentException, System.SystemException,
		System.IDisposable, System.BitConverter, System.ByteOrder,
		System.LengthPrefix, System.EventManager, System.ICloneable;
}