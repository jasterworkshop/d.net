# README #

### Overview ###

This goal of this library is to give D developers access to a framework that is similar to the .Net framework.

For the classes currently implemented, the interfaces are generally the same, however some notable classes that are very different from their .Net counterparts are:

* **System.BitConverter** as support for choosing endianness was added
* **System.IO.BinaryWriter & BinaryReader** also due to endianness support

### How do I get set up? ###

All you need to get started is [dub](http://code.dlang.org/download).

First, make a new dub project by typing into your console

```
#!

dub init ProjectName
```
A new directory called *ProjectName* should appear, inside that directory should be a file called *dub.json*.
Inside this file, change the *"dependencies"* to look like this:

```
#!json

"dependencies": {
	"d-net": ">=0.2"
}
```
Finally, with a console open in the directory that contains the json file, use the following command to make sure everything's setup correctly:

```
#!

dub
```

D.net doesn't rely on any external dependencies other than Phobos, D's standard library.

### Hierarchy ###

* System - **TODO**
    * System.IO - Contains classes involving IO
    * System.Extra - Anything extra that I feel would be useful to have(**NOTE** any module under here has a decent chance to become deprecated, as I'm terrible at making things sometimes)
    * System.Collections - **TODO**

### TODO ###

* Add pure, nothrow, const, etc. to functions
* Mark certain parameters as *in*
* Try to make example programs for each class and fix up anything that feels clunky to use.